﻿
namespace ProgettoDB
{
    partial class wishlist
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.selettore_prodotto = new System.Windows.Forms.ComboBox();
            this.remove_product = new System.Windows.Forms.Button();
            this.svuota_wishlist = new System.Windows.Forms.Button();
            this.refresh = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(235, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Wishlist";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(133, 40);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(409, 327);
            this.dataGridView1.TabIndex = 2;
            // 
            // selettore_prodotto
            // 
            this.selettore_prodotto.FormattingEnabled = true;
            this.selettore_prodotto.Location = new System.Drawing.Point(6, 83);
            this.selettore_prodotto.Name = "selettore_prodotto";
            this.selettore_prodotto.Size = new System.Drawing.Size(121, 21);
            this.selettore_prodotto.TabIndex = 3;
            // 
            // remove_product
            // 
            this.remove_product.Location = new System.Drawing.Point(6, 111);
            this.remove_product.Name = "remove_product";
            this.remove_product.Size = new System.Drawing.Size(121, 40);
            this.remove_product.TabIndex = 4;
            this.remove_product.Text = "Rimuovi questo prodotto dalla wishlist";
            this.remove_product.UseVisualStyleBackColor = true;
            this.remove_product.Click += new System.EventHandler(this.remove_product_Click);
            // 
            // svuota_wishlist
            // 
            this.svuota_wishlist.Location = new System.Drawing.Point(6, 157);
            this.svuota_wishlist.Name = "svuota_wishlist";
            this.svuota_wishlist.Size = new System.Drawing.Size(121, 42);
            this.svuota_wishlist.TabIndex = 6;
            this.svuota_wishlist.Text = "Svuota wishlist";
            this.svuota_wishlist.UseVisualStyleBackColor = true;
            this.svuota_wishlist.Click += new System.EventHandler(this.svuota_wishlist_Click);
            // 
            // refresh
            // 
            this.refresh.Location = new System.Drawing.Point(6, 205);
            this.refresh.Name = "refresh";
            this.refresh.Size = new System.Drawing.Size(121, 23);
            this.refresh.TabIndex = 39;
            this.refresh.Text = "Refresh";
            this.refresh.UseVisualStyleBackColor = true;
            this.refresh.Click += new System.EventHandler(this.refresh_Click);
            // 
            // wishlist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.refresh);
            this.Controls.Add(this.svuota_wishlist);
            this.Controls.Add(this.remove_product);
            this.Controls.Add(this.selettore_prodotto);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label1);
            this.Name = "wishlist";
            this.Size = new System.Drawing.Size(545, 370);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox selettore_prodotto;
        private System.Windows.Forms.Button remove_product;
        private System.Windows.Forms.Button svuota_wishlist;
        private System.Windows.Forms.Button refresh;
    }
}
