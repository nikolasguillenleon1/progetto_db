﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace ProgettoDB
{
    public partial class modifica_prodotti : UserControl
    {
        DataClasse_progettoDBDataContext db = new DataClasse_progettoDBDataContext();
        Random rnd = new Random();

        public modifica_prodotti()
        {
            InitializeComponent();
            selettore_prodotto.DropDownStyle = ComboBoxStyle.DropDownList;
            selettore_prodotto.DataSource = from p in db.PRODOTTO
                                            select p.IDprodotto;

            prezzo_input.Maximum = 9999999;
            scorta_input.Maximum = 9999999;
            eta_input.Maximum = 9999999;

            var categorie = from c in db.CATEGORIA
                            select c.nome;

            var index = 0;
            foreach (var c in categorie)
            {
                if (index == 0)
                {
                    categoria_1.Text = c;
                }
                else
                {
                    categoria_2.Text = c;
                }
                index++;
            }

            appartenenza a_1 = new appartenenza()
            {
                IDcategoria = "1",
                IDprodotto = selettore_prodotto.Text,
            };

            if (db.appartenenza.Contains(a_1))
            {
                categoria_1.Checked = true;
            }

            appartenenza a_2 = new appartenenza()
            {
                IDcategoria = "2",
                IDprodotto = selettore_prodotto.Text,
            };

            if (db.appartenenza.Contains(a_2))
            {
                categoria_2.Checked = true;
            }
        }

        private void refresh_Click(object sender, EventArgs e)
        {
            selettore_prodotto.DataSource = from p in db.PRODOTTO
                                            select p.IDprodotto;

            var elenco_categorie = from a in db.appartenenza
                                   from c in db.CATEGORIA
                                   where a.IDprodotto == selettore_prodotto.Text
                                   where c.IDcategoria == a.IDcategoria
                                   select c;

            foreach (var cat in elenco_categorie)
            {
                if (cat.nome == categoria_1.Text)
                {
                    categoria_1.Checked = true;
                }

                if (cat.nome == categoria_2.Text)
                {
                    categoria_2.Checked = true;
                }
            }
        }

        private void salva_modifiche_Click(object sender, EventArgs e)
        {
            var update = db.PRODOTTO.First(g => g.IDprodotto == selettore_prodotto.Text);

            if(update.quantita_scorta == 0 && scorta_input.Value > 0)
            {
                var lista_utenti = from w in db.WISHLIST
                                   from cw in db.composizione_wishlist
                                   from u in db.UTENTE
                                   where cw.IDprodotto == update.IDprodotto
                                   where w.IDwishlist == cw.IDwishlist
                                   where u.codice == w.codice_utente
                                   select u;

                NOTIFICA notifica_disponibilità = new NOTIFICA
                {
                    IDnotifica = rnd.Next(1, 99999).ToString(),
                    titolo = "Prodotto nuovamente disponibile!",
                    messaggio = "Il prodotto \"" + update.nome_prodotto.ToString() + "\" è nuovamente disponibile.",
                    data = DateTime.Today,
                    visualizzato = '0',
                };

                db.NOTIFICA.InsertOnSubmit(notifica_disponibilità);

                db.SubmitChanges();

                foreach(var utente in lista_utenti)
                {
                    ricezione ricezione_notifca = new ricezione
                    {
                        IDnotifica = notifica_disponibilità.IDnotifica,
                        codice_utente = utente.codice,
                    };

                    db.ricezione.InsertOnSubmit(ricezione_notifca);
                }

                db.SubmitChanges();

                riferimento_notifica riferimeto_n = new riferimento_notifica
                {
                    IDnotifica = notifica_disponibilità.IDnotifica,
                    IDprodotto = update.IDprodotto,
                };

                db.riferimento_notifica.InsertOnSubmit(riferimeto_n);
                
            }

            update.nome_prodotto = nome_prodotto_text.Text;
            update.prezzo_unitario = prezzo_input.Value;
            update.quantita_scorta = scorta_input.Value;
            update.taglia = taglia_text.Text;
            update.eta = eta_input.Value;
            update.descrizione = descrizione_text.Text;

            var elenco_appartenenze = from app in db.appartenenza
                                      where app.IDprodotto == update.IDprodotto
                                      select app;


            // Check categoria 1
            if (categoria_1.Checked == true)
            {
                appartenenza a = new appartenenza
                {
                    IDcategoria = "1",
                    IDprodotto = update.IDprodotto,
                };
                if (!db.appartenenza.Contains(a))
                {
                    db.appartenenza.InsertOnSubmit(a);
                }
            }
            else if (categoria_1.Checked == false)
            {
                var a = (from app in db.appartenenza
                         where app.IDcategoria == "1" && app.IDprodotto == update.IDprodotto
                         select app).FirstOrDefault();

                if (db.appartenenza.Contains(a))
                {
                    db.appartenenza.DeleteOnSubmit(a);
                }            
            }

            // Check categoria 2
            if (categoria_2.Checked == true)
            {
                appartenenza a = new appartenenza
                {
                    IDcategoria = "2",
                    IDprodotto = update.IDprodotto,
                };
                if (!db.appartenenza.Contains(a))
                {
                    db.appartenenza.InsertOnSubmit(a);
                }
            }
            else if (categoria_2.Checked == false)
            {
                var a = (from app in db.appartenenza
                         where app.IDcategoria == "2" && app.IDprodotto == update.IDprodotto
                         select app).FirstOrDefault();

                if (db.appartenenza.Contains(a))
                {
                    db.appartenenza.DeleteOnSubmit(a);
                }
            }

            db.SubmitChanges();

            refresh.PerformClick();
        }

        private void selettore_prodotto_SelectedIndexChanged(object sender, EventArgs e)
        {
            categoria_1.Checked = false;
            categoria_2.Checked = false;

            var prodotto_selezionato = (from p in db.PRODOTTO
                                        where p.IDprodotto == selettore_prodotto.Text
                                        select p).FirstOrDefault();

            nome_prodotto_text.Text = prodotto_selezionato.nome_prodotto;
            prezzo_input.Value = prodotto_selezionato.prezzo_unitario;
            scorta_input.Value = prodotto_selezionato.quantita_scorta;
            taglia_text.Text = prodotto_selezionato.taglia;
            eta_input.Value = prodotto_selezionato.eta;
            descrizione_text.Text = prodotto_selezionato.descrizione;


            appartenenza a_1 = new appartenenza()
            {
                IDcategoria = "1",
                IDprodotto = prodotto_selezionato.IDprodotto,
            };

            if(db.appartenenza.Contains(a_1))
            {
                categoria_1.Checked = true;
            }

            appartenenza a_2 = new appartenenza()
            {
                IDcategoria = "2",
                IDprodotto = prodotto_selezionato.IDprodotto,
            };

            if (db.appartenenza.Contains(a_2))
            {
                categoria_2.Checked = true;
            }
        }
    }
}
