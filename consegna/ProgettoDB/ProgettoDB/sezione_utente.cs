﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace ProgettoDB
{
    public partial class sezione_utente : UserControl
    {
        DataClasse_progettoDBDataContext db = new DataClasse_progettoDBDataContext();

        public sezione_utente()
        {
            InitializeComponent();
        }

        private void sezione_utente_Load(object sender, EventArgs e)
        {

        }

        private void visualizza_ordini_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = from o in db.ORDINE
                                       from u in db.UTENTE
                                       join f in db.FATTURA on u.codice equals f.codice_utente
                                       where f.codice_utente == "000000001"
                                       where o.IDordine == f.IDordine
                                       select o;
        }

        private void visualizza_fatture_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = from f in db.FATTURA
                                       where f.codice_utente == "000000001"
                                       select f;
        }

        private void visualizza_notifiche_Click(object sender, EventArgs e)
        {
            var notifiche = from n in db.NOTIFICA
                            from r in db.ricezione
                            where r.codice_utente == "000000001"
                            where n.IDnotifica == r.IDnotifica
                            select n;
            
            foreach(var notifica in notifiche)
            {
                notifica.visualizzato = '1';
            }

            dataGridView1.DataSource = from n in db.NOTIFICA
                                       from r in db.ricezione
                                       where r.codice_utente == "000000001"
                                       where n.IDnotifica == r.IDnotifica
                                       select new { n.titolo, n.messaggio, n.data };
        }

        private void visualizza_recensioni_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = from rec in db.RECENSIONE
                                       where rec.codice_utente == "000000001"
                                       select rec;
        }

        private void filtra_ordini_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = from o in db.ORDINE
                                       from u in db.UTENTE
                                       join f in db.FATTURA on u.codice equals f.codice_utente
                                       where f.codice_utente == "000000001"
                                       where o.IDordine == f.IDordine
                                       where o.data_ordine >= data_inizio.Value && o.data_ordine <= data_fine.Value
                                       select o;
        }

        private void rimuovi_filtri_Click(object sender, EventArgs e)
        {
            visualizza_ordini.PerformClick();
        }
    }
}
