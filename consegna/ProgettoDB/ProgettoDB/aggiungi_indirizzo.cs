﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace ProgettoDB
{
    public partial class aggiungi_indirizzo : UserControl
    {
        DataClasse_progettoDBDataContext db = new DataClasse_progettoDBDataContext();
        Random rnd = new Random();
        public aggiungi_indirizzo()
        {
            InitializeComponent();

            dataGridView1.DataSource = from i in db.INDIRIZZO
                                       from ii in db.intestazione_indirizzo
                                       where ii.codice_utente == "000000001"
                                       where i.IDindirizzo == ii.IDindirizzo
                                       select i;

            selettore_indirizzo.DropDownStyle = ComboBoxStyle.DropDownList;
            selettore_indirizzo.DataSource = from i in db.INDIRIZZO
                                             from ii in db.intestazione_indirizzo
                                             where ii.codice_utente == "000000001"
                                             where i.IDindirizzo == ii.IDindirizzo
                                             select i.IDindirizzo;
        }

        private void refresh_view()
        {
            dataGridView1.DataSource = from i in db.INDIRIZZO
                                       from ii in db.intestazione_indirizzo
                                       where ii.codice_utente == "000000001"
                                       where i.IDindirizzo == ii.IDindirizzo
                                       select i;

            selettore_indirizzo.DataSource = from i in db.INDIRIZZO
                                             from ii in db.intestazione_indirizzo
                                             where ii.codice_utente == "000000001"
                                             where i.IDindirizzo == ii.IDindirizzo
                                             select i.IDindirizzo;
        }

        private void aggiunta_indirizzo_Click(object sender, EventArgs e)
        {
            INDIRIZZO indirizzo = new INDIRIZZO
            {
                IDindirizzo = rnd.Next(1, 10000).ToString(),
                via = box_via.Text,
                citta = box_citta.Text,
                CAP = box_cap.Value,
                nazione = box_nazione.Text,
            };

            intestazione_indirizzo intestazione = new intestazione_indirizzo
            {
                IDindirizzo = indirizzo.IDindirizzo,
                codice_utente = "000000001",
            };

            db.INDIRIZZO.InsertOnSubmit(indirizzo);
            db.intestazione_indirizzo.InsertOnSubmit(intestazione);

            db.SubmitChanges();
            refresh_view();
        }

        private void rimuovi_indirizzo_Click(object sender, EventArgs e)
        {

            var intestazione = (from i in db.intestazione_indirizzo
                                where i.IDindirizzo == selettore_indirizzo.Text && i.codice_utente == "000000001"
                                select i).FirstOrDefault();

            db.intestazione_indirizzo.DeleteOnSubmit(intestazione);
            db.SubmitChanges();

            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.intestazione_indirizzo);

            var is_present = (from ii in db.intestazione_indirizzo
                              where ii.IDindirizzo == selettore_indirizzo.Text
                              select ii).FirstOrDefault();

            if(is_present == null)
            {
                var indirizzo = (from i in db.INDIRIZZO
                                 where i.IDindirizzo == selettore_indirizzo.Text
                                 select i).FirstOrDefault();

                db.INDIRIZZO.DeleteOnSubmit(indirizzo);
                db.SubmitChanges();
            }

            refresh_view();
        }
    }
}