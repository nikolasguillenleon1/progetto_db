﻿
namespace ProgettoDB
{
    partial class modifica_prodotti
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.selettore_prodotto = new System.Windows.Forms.ComboBox();
            this.nome_prodotto_text = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.prezzo = new System.Windows.Forms.Label();
            this.scorta = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.taglia_text = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.descrizione_text = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.salva_modifiche = new System.Windows.Forms.Button();
            this.refresh = new System.Windows.Forms.Button();
            this.prezzo_input = new System.Windows.Forms.NumericUpDown();
            this.scorta_input = new System.Windows.Forms.NumericUpDown();
            this.eta_input = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.categoria_2 = new System.Windows.Forms.CheckBox();
            this.categoria_1 = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.prezzo_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scorta_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eta_input)).BeginInit();
            this.SuspendLayout();
            // 
            // selettore_prodotto
            // 
            this.selettore_prodotto.FormattingEnabled = true;
            this.selettore_prodotto.Location = new System.Drawing.Point(6, 86);
            this.selettore_prodotto.Name = "selettore_prodotto";
            this.selettore_prodotto.Size = new System.Drawing.Size(530, 21);
            this.selettore_prodotto.TabIndex = 0;
            this.selettore_prodotto.SelectedIndexChanged += new System.EventHandler(this.selettore_prodotto_SelectedIndexChanged);
            // 
            // nome_prodotto_text
            // 
            this.nome_prodotto_text.Location = new System.Drawing.Point(6, 135);
            this.nome_prodotto_text.Name = "nome_prodotto_text";
            this.nome_prodotto_text.Size = new System.Drawing.Size(254, 20);
            this.nome_prodotto_text.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 119);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Nome prodotto";
            // 
            // prezzo
            // 
            this.prezzo.AutoSize = true;
            this.prezzo.Location = new System.Drawing.Point(3, 158);
            this.prezzo.Name = "prezzo";
            this.prezzo.Size = new System.Drawing.Size(76, 13);
            this.prezzo.TabIndex = 10;
            this.prezzo.Text = "Prezzo unitario";
            // 
            // scorta
            // 
            this.scorta.AutoSize = true;
            this.scorta.Location = new System.Drawing.Point(3, 197);
            this.scorta.Name = "scorta";
            this.scorta.Size = new System.Drawing.Size(79, 13);
            this.scorta.TabIndex = 12;
            this.scorta.Text = "Quantità scorta";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 236);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Descrizione";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(276, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Taglia";
            // 
            // taglia_text
            // 
            this.taglia_text.Location = new System.Drawing.Point(279, 135);
            this.taglia_text.Name = "taglia_text";
            this.taglia_text.Size = new System.Drawing.Size(257, 20);
            this.taglia_text.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(276, 158);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Età";
            // 
            // descrizione_text
            // 
            this.descrizione_text.Location = new System.Drawing.Point(6, 252);
            this.descrizione_text.Name = "descrizione_text";
            this.descrizione_text.Size = new System.Drawing.Size(530, 70);
            this.descrizione_text.TabIndex = 6;
            this.descrizione_text.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(188, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(171, 25);
            this.label2.TabIndex = 20;
            this.label2.Text = "Modifica prodotti";
            // 
            // salva_modifiche
            // 
            this.salva_modifiche.Location = new System.Drawing.Point(6, 329);
            this.salva_modifiche.Name = "salva_modifiche";
            this.salva_modifiche.Size = new System.Drawing.Size(530, 38);
            this.salva_modifiche.TabIndex = 7;
            this.salva_modifiche.Text = "Salva modifiche";
            this.salva_modifiche.UseVisualStyleBackColor = true;
            this.salva_modifiche.Click += new System.EventHandler(this.salva_modifiche_Click);
            // 
            // refresh
            // 
            this.refresh.Location = new System.Drawing.Point(6, 34);
            this.refresh.Name = "refresh";
            this.refresh.Size = new System.Drawing.Size(76, 23);
            this.refresh.TabIndex = 22;
            this.refresh.Text = "Refresh";
            this.refresh.UseVisualStyleBackColor = true;
            this.refresh.Click += new System.EventHandler(this.refresh_Click);
            // 
            // prezzo_input
            // 
            this.prezzo_input.Location = new System.Drawing.Point(6, 175);
            this.prezzo_input.Name = "prezzo_input";
            this.prezzo_input.Size = new System.Drawing.Size(254, 20);
            this.prezzo_input.TabIndex = 2;
            // 
            // scorta_input
            // 
            this.scorta_input.Location = new System.Drawing.Point(6, 213);
            this.scorta_input.Name = "scorta_input";
            this.scorta_input.Size = new System.Drawing.Size(254, 20);
            this.scorta_input.TabIndex = 3;
            // 
            // eta_input
            // 
            this.eta_input.Location = new System.Drawing.Point(279, 175);
            this.eta_input.Name = "eta_input";
            this.eta_input.Size = new System.Drawing.Size(257, 20);
            this.eta_input.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "Seleziona il prodotto";
            // 
            // categoria_2
            // 
            this.categoria_2.AutoSize = true;
            this.categoria_2.Location = new System.Drawing.Point(385, 216);
            this.categoria_2.Name = "categoria_2";
            this.categoria_2.Size = new System.Drawing.Size(80, 17);
            this.categoria_2.TabIndex = 62;
            this.categoria_2.Text = "checkBox2";
            this.categoria_2.UseVisualStyleBackColor = true;
            // 
            // categoria_1
            // 
            this.categoria_1.AutoSize = true;
            this.categoria_1.Location = new System.Drawing.Point(279, 216);
            this.categoria_1.Name = "categoria_1";
            this.categoria_1.Size = new System.Drawing.Size(80, 17);
            this.categoria_1.TabIndex = 61;
            this.categoria_1.Text = "checkBox1";
            this.categoria_1.UseVisualStyleBackColor = true;
            // 
            // modifica_prodotti
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.categoria_2);
            this.Controls.Add(this.categoria_1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.eta_input);
            this.Controls.Add(this.scorta_input);
            this.Controls.Add(this.prezzo_input);
            this.Controls.Add(this.refresh);
            this.Controls.Add(this.salva_modifiche);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.descrizione_text);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.taglia_text);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.scorta);
            this.Controls.Add(this.prezzo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nome_prodotto_text);
            this.Controls.Add(this.selettore_prodotto);
            this.Name = "modifica_prodotti";
            this.Size = new System.Drawing.Size(545, 370);
            ((System.ComponentModel.ISupportInitialize)(this.prezzo_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scorta_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eta_input)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox selettore_prodotto;
        private System.Windows.Forms.TextBox nome_prodotto_text;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label prezzo;
        private System.Windows.Forms.Label scorta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox taglia_text;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox descrizione_text;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button salva_modifiche;
        private System.Windows.Forms.Button refresh;
        private System.Windows.Forms.NumericUpDown prezzo_input;
        private System.Windows.Forms.NumericUpDown scorta_input;
        private System.Windows.Forms.NumericUpDown eta_input;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox categoria_2;
        private System.Windows.Forms.CheckBox categoria_1;
    }
}
