﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace ProgettoDB
{
    public partial class dettaglio_prodotto_1 : UserControl
    {
        DataClasse_progettoDBDataContext db = new DataClasse_progettoDBDataContext();
        Random rnd = new Random();

        public dettaglio_prodotto_1()
        {
            InitializeComponent();
            recensioni_grid.DataSource = from r in db.RECENSIONE
                                         where r.IDprodotto == "06A0000"
                                         select new { r.punteggio, r.titolo, r.descrizione, r.UTENTE.nome };
        }

        private void aggiunta_carrello_Click(object sender, EventArgs e)
        {
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.composizione_carrello);

            // Si può fare anche assegnando direttamente l'ID 06A0000 alla variabile prodotto
            var prodotto = (from p in db.PRODOTTO
                            where p.IDprodotto == "06A0000"
                            select p).FirstOrDefault();

            var IDcarrello = (from id in db.CARRELLO
                              where id.codice_utente == "000000001"
                              select id.IDcarrello).FirstOrDefault();

            if (quantita_input.Value <= prodotto.quantita_scorta)
            {
                var cc_1 = (from c in db.composizione_carrello
                            where c.IDcarrello == IDcarrello && c.IDprodotto == prodotto.IDprodotto
                            select c).FirstOrDefault();

                // Controllo se il prodotto è già nel carrello
                if (!db.composizione_carrello.Contains(cc_1))
                {
                    composizione_carrello cc = new composizione_carrello
                    {
                        IDprodotto = prodotto.IDprodotto,
                        IDcarrello = IDcarrello,
                        quantita = quantita_input.Value,
                    };

                    db.composizione_carrello.InsertOnSubmit(cc);
                    db.SubmitChanges();

                    var update_totale = db.CARRELLO.First(g => g.IDcarrello == IDcarrello);

                    update_totale.totale = update_totale.totale + ((prodotto.prezzo_unitario) * quantita_input.Value);
                    db.SubmitChanges();
                }
                else
                {
                    var cc = (from c in db.composizione_carrello
                              where c.IDcarrello == IDcarrello
                              select c).FirstOrDefault();

                    cc.quantita = cc.quantita + quantita_input.Value;
                    db.SubmitChanges();

                    var update_totale = db.CARRELLO.First(g => g.IDcarrello == IDcarrello);

                    update_totale.totale = update_totale.totale + ((prodotto.prezzo_unitario) * quantita_input.Value);

                    db.SubmitChanges();
                }
            }
        }

        private void aggiunta_wishlist_Click(object sender, EventArgs e)
        {
            var prodotto = "06A0000";

            var IDwishlist = (from id in db.WISHLIST
                              where id.codice_utente == "000000001"
                              select id.IDwishlist).FirstOrDefault();

            composizione_wishlist cw = new composizione_wishlist
            {
                IDprodotto = prodotto,
                IDwishlist = IDwishlist,
            };

            db.composizione_wishlist.InsertOnSubmit(cw);
            db.SubmitChanges();
        }

        private void pubblica_recensione_Click(object sender, EventArgs e)
        {

            RECENSIONE recensione = new RECENSIONE
            {
                IDrecensione = rnd.Next(1, 9999).ToString(),
                IDprodotto = "06A0000",
                titolo = titolo_box.Text,
                descrizione = recensione_box.Text,
                punteggio = punteggio_input.Value,
                codice_utente = "000000001",
            };

            db.RECENSIONE.InsertOnSubmit(recensione);
            db.SubmitChanges();
        }

        private void aggiorna_recensioni_Click(object sender, EventArgs e)
        {
            recensioni_grid.DataSource = from r in db.RECENSIONE
                                         where r.IDprodotto == "06A0000"
                                         select new { r.punteggio, r.titolo, r.descrizione, recensore = r.UTENTE.nome };
        }
    }
}
