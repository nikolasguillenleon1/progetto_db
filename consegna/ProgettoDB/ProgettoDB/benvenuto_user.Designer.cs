﻿
namespace ProgettoDB
{
    partial class benvenuto_user
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.to_carrello = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.to_user = new System.Windows.Forms.Button();
            this.to_shop = new System.Windows.Forms.Button();
            this.logout = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Benvenuto!";
            // 
            // to_carrello
            // 
            this.to_carrello.Location = new System.Drawing.Point(11, 141);
            this.to_carrello.Name = "to_carrello";
            this.to_carrello.Size = new System.Drawing.Size(145, 73);
            this.to_carrello.TabIndex = 11;
            this.to_carrello.Text = "Vai al carrello";
            this.to_carrello.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(11, 220);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(145, 73);
            this.button1.TabIndex = 13;
            this.button1.Text = "Vai alla wishlist";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // to_user
            // 
            this.to_user.Location = new System.Drawing.Point(11, 299);
            this.to_user.Name = "to_user";
            this.to_user.Size = new System.Drawing.Size(143, 73);
            this.to_user.TabIndex = 14;
            this.to_user.Text = "Vai alla sezione utente";
            this.to_user.UseVisualStyleBackColor = true;
            // 
            // to_shop
            // 
            this.to_shop.Location = new System.Drawing.Point(11, 62);
            this.to_shop.Name = "to_shop";
            this.to_shop.Size = new System.Drawing.Size(145, 73);
            this.to_shop.TabIndex = 15;
            this.to_shop.Text = "Vai al negozio";
            this.to_shop.UseVisualStyleBackColor = true;
            // 
            // logout
            // 
            this.logout.Location = new System.Drawing.Point(11, 373);
            this.logout.Name = "logout";
            this.logout.Size = new System.Drawing.Size(143, 23);
            this.logout.TabIndex = 16;
            this.logout.Text = "Logout";
            this.logout.UseVisualStyleBackColor = true;
            // 
            // benvenuto_user
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.logout);
            this.Controls.Add(this.to_shop);
            this.Controls.Add(this.to_user);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.to_carrello);
            this.Controls.Add(this.label1);
            this.Name = "benvenuto_user";
            this.Size = new System.Drawing.Size(183, 396);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button to_carrello;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button to_user;
        private System.Windows.Forms.Button to_shop;
        private System.Windows.Forms.Button logout;
    }
}
