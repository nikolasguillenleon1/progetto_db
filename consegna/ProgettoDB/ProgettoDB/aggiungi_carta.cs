﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgettoDB
{
    public partial class aggiungi_carta : UserControl
    {
        DataClasse_progettoDBDataContext db = new DataClasse_progettoDBDataContext();

        public aggiungi_carta()
        {
            InitializeComponent();
            dataGridView1.DataSource = from c in db.CARTA_DI_CREDITO
                                       from ic in db.intestazione_carta
                                       where ic.codice_utente == "000000001"
                                       where c.numero_carta == ic.numero_carta
                                       select c;

            selettore_carta.DropDownStyle = ComboBoxStyle.DropDownList;
            selettore_carta.DataSource = from c in db.CARTA_DI_CREDITO
                                         from ic in db.intestazione_carta
                                         where ic.codice_utente == "000000001"
                                         where c.numero_carta == ic.numero_carta
                                         select c.numero_carta;
        }

        private void refresh_view()
        {
            dataGridView1.DataSource = from c in db.CARTA_DI_CREDITO
                                       from ic in db.intestazione_carta
                                       where ic.codice_utente == "000000001"
                                       where c.numero_carta == ic.numero_carta
                                       select c;

            selettore_carta.DataSource = from c in db.CARTA_DI_CREDITO
                                         from ic in db.intestazione_carta
                                         where ic.codice_utente == "000000001"
                                         where c.numero_carta == ic.numero_carta
                                         select c.numero_carta;
        }

        private void nuova_carta_Click(object sender, EventArgs e)
        {
            CARTA_DI_CREDITO carta = new CARTA_DI_CREDITO
            {
                numero_carta = long.Parse(box_carta.Text),
                data_scadenza = box_scadenza.Text,
                cvv = long.Parse(box_cvv.Text),
            };

            intestazione_carta intestazione = new intestazione_carta
            {
                numero_carta = carta.numero_carta,
                codice_utente = "000000001",
            };

            db.CARTA_DI_CREDITO.InsertOnSubmit(carta);
            db.intestazione_carta.InsertOnSubmit(intestazione);

            db.SubmitChanges();
            refresh_view();
        }

        private void rimuovi_carta_Click(object sender, EventArgs e)
        {
            var intestazione = (from i in db.intestazione_carta
                                where i.numero_carta == long.Parse(selettore_carta.Text) && i.codice_utente == "000000001"
                                select i).FirstOrDefault();

            db.intestazione_carta.DeleteOnSubmit(intestazione);
            db.SubmitChanges();

            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.intestazione_indirizzo);

            var is_present = (from ii in db.intestazione_carta
                              where ii.numero_carta == long.Parse(selettore_carta.Text)
                              select ii).FirstOrDefault();

            if (is_present == null)
            {
                var carta = (from i in db.CARTA_DI_CREDITO
                             where i.numero_carta == long.Parse(selettore_carta.Text)
                             select i).FirstOrDefault();

                db.CARTA_DI_CREDITO.DeleteOnSubmit(carta);
                db.SubmitChanges();
            }

            refresh_view();
        }
    }
}
