﻿
namespace ProgettoDB
{
    partial class rimozione_prodotti
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.selettore_rimozione = new System.Windows.Forms.ComboBox();
            this.rimuovi_prodotto = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.refresh = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // selettore_rimozione
            // 
            this.selettore_rimozione.FormattingEnabled = true;
            this.selettore_rimozione.Location = new System.Drawing.Point(3, 77);
            this.selettore_rimozione.Name = "selettore_rimozione";
            this.selettore_rimozione.Size = new System.Drawing.Size(138, 21);
            this.selettore_rimozione.TabIndex = 0;
            // 
            // rimuovi_prodotto
            // 
            this.rimuovi_prodotto.Location = new System.Drawing.Point(3, 104);
            this.rimuovi_prodotto.Name = "rimuovi_prodotto";
            this.rimuovi_prodotto.Size = new System.Drawing.Size(138, 23);
            this.rimuovi_prodotto.TabIndex = 1;
            this.rimuovi_prodotto.Text = "Rimuovi prodotto";
            this.rimuovi_prodotto.UseVisualStyleBackColor = true;
            this.rimuovi_prodotto.Click += new System.EventHandler(this.rimuovi_prodotto_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(147, 77);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(395, 290);
            this.dataGridView1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(171, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(190, 25);
            this.label2.TabIndex = 37;
            this.label2.Text = "Rimozione prodotti";
            // 
            // refresh
            // 
            this.refresh.Location = new System.Drawing.Point(3, 133);
            this.refresh.Name = "refresh";
            this.refresh.Size = new System.Drawing.Size(138, 23);
            this.refresh.TabIndex = 38;
            this.refresh.Text = "Refresh";
            this.refresh.UseVisualStyleBackColor = true;
            this.refresh.Click += new System.EventHandler(this.refresh_Click);
            // 
            // rimozione_prodotti
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.refresh);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.rimuovi_prodotto);
            this.Controls.Add(this.selettore_rimozione);
            this.Name = "rimozione_prodotti";
            this.Size = new System.Drawing.Size(545, 370);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox selettore_rimozione;
        private System.Windows.Forms.Button rimuovi_prodotto;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button refresh;
    }
}
