﻿
namespace ProgettoDB
{
    partial class aggiungi_carta
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.rimuovi_carta = new System.Windows.Forms.Button();
            this.selettore_carta = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.box_cvv = new System.Windows.Forms.TextBox();
            this.box_scadenza = new System.Windows.Forms.TextBox();
            this.box_carta = new System.Windows.Forms.TextBox();
            this.nuova_carta = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(200, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 25);
            this.label5.TabIndex = 39;
            this.label5.Text = "Gestione carte";
            // 
            // rimuovi_carta
            // 
            this.rimuovi_carta.Location = new System.Drawing.Point(16, 251);
            this.rimuovi_carta.Name = "rimuovi_carta";
            this.rimuovi_carta.Size = new System.Drawing.Size(100, 51);
            this.rimuovi_carta.TabIndex = 38;
            this.rimuovi_carta.Text = "Rimuovi questa carta";
            this.rimuovi_carta.UseVisualStyleBackColor = true;
            this.rimuovi_carta.Click += new System.EventHandler(this.rimuovi_carta_Click);
            // 
            // selettore_carta
            // 
            this.selettore_carta.FormattingEnabled = true;
            this.selettore_carta.Location = new System.Drawing.Point(16, 224);
            this.selettore_carta.Name = "selettore_carta";
            this.selettore_carta.Size = new System.Drawing.Size(100, 21);
            this.selettore_carta.TabIndex = 37;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(154, 43);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(378, 313);
            this.dataGridView1.TabIndex = 36;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 13);
            this.label3.TabIndex = 35;
            this.label3.Text = "Codice di sicurezza (CVV)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 34;
            this.label2.Text = "Data di scadenza";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 33;
            this.label1.Text = "Numero carta";
            // 
            // box_cvv
            // 
            this.box_cvv.Location = new System.Drawing.Point(16, 135);
            this.box_cvv.Name = "box_cvv";
            this.box_cvv.Size = new System.Drawing.Size(100, 20);
            this.box_cvv.TabIndex = 32;
            // 
            // box_scadenza
            // 
            this.box_scadenza.Location = new System.Drawing.Point(16, 96);
            this.box_scadenza.Name = "box_scadenza";
            this.box_scadenza.Size = new System.Drawing.Size(100, 20);
            this.box_scadenza.TabIndex = 31;
            // 
            // box_carta
            // 
            this.box_carta.Location = new System.Drawing.Point(16, 59);
            this.box_carta.Name = "box_carta";
            this.box_carta.Size = new System.Drawing.Size(100, 20);
            this.box_carta.TabIndex = 30;
            // 
            // nuova_carta
            // 
            this.nuova_carta.Location = new System.Drawing.Point(16, 161);
            this.nuova_carta.Name = "nuova_carta";
            this.nuova_carta.Size = new System.Drawing.Size(100, 28);
            this.nuova_carta.TabIndex = 29;
            this.nuova_carta.Text = "Aggiungi carta";
            this.nuova_carta.UseVisualStyleBackColor = true;
            this.nuova_carta.Click += new System.EventHandler(this.nuova_carta_Click);
            // 
            // aggiungi_carta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label5);
            this.Controls.Add(this.rimuovi_carta);
            this.Controls.Add(this.selettore_carta);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.box_cvv);
            this.Controls.Add(this.box_scadenza);
            this.Controls.Add(this.box_carta);
            this.Controls.Add(this.nuova_carta);
            this.Name = "aggiungi_carta";
            this.Size = new System.Drawing.Size(545, 370);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button rimuovi_carta;
        private System.Windows.Forms.ComboBox selettore_carta;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox box_cvv;
        private System.Windows.Forms.TextBox box_scadenza;
        private System.Windows.Forms.TextBox box_carta;
        private System.Windows.Forms.Button nuova_carta;
    }
}
