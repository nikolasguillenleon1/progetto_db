﻿
namespace ProgettoDB
{
    partial class login
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.errore_login_admin = new System.Windows.Forms.Label();
            this.login_admin = new System.Windows.Forms.Button();
            this.login_user = new System.Windows.Forms.Button();
            this.email_admin = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.password_admin = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(59, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Password";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(50, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 20);
            this.label1.TabIndex = 17;
            this.label1.Text = "ADMIN";
            // 
            // errore_login_admin
            // 
            this.errore_login_admin.AutoSize = true;
            this.errore_login_admin.Location = new System.Drawing.Point(19, 176);
            this.errore_login_admin.Name = "errore_login_admin";
            this.errore_login_admin.Size = new System.Drawing.Size(145, 13);
            this.errore_login_admin.TabIndex = 21;
            this.errore_login_admin.Text = "Username o password errate.";
            // 
            // login_admin
            // 
            this.login_admin.Location = new System.Drawing.Point(36, 135);
            this.login_admin.Name = "login_admin";
            this.login_admin.Size = new System.Drawing.Size(100, 23);
            this.login_admin.TabIndex = 14;
            this.login_admin.Text = "LOGIN";
            this.login_admin.UseVisualStyleBackColor = true;
            this.login_admin.Click += new System.EventHandler(this.login_admin_Click);
            // 
            // login_user
            // 
            this.login_user.Location = new System.Drawing.Point(36, 273);
            this.login_user.Name = "login_user";
            this.login_user.Size = new System.Drawing.Size(100, 23);
            this.login_user.TabIndex = 20;
            this.login_user.Text = "LOGIN";
            this.login_user.UseVisualStyleBackColor = true;
            // 
            // email_admin
            // 
            this.email_admin.Location = new System.Drawing.Point(36, 66);
            this.email_admin.Name = "email_admin";
            this.email_admin.Size = new System.Drawing.Size(100, 20);
            this.email_admin.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(53, 250);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 20);
            this.label3.TabIndex = 19;
            this.label3.Text = "USER";
            // 
            // password_admin
            // 
            this.password_admin.Location = new System.Drawing.Point(36, 106);
            this.password_admin.Name = "password_admin";
            this.password_admin.PasswordChar = '*';
            this.password_admin.Size = new System.Drawing.Size(100, 20);
            this.password_admin.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(67, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Email";
            // 
            // login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.errore_login_admin);
            this.Controls.Add(this.login_admin);
            this.Controls.Add(this.login_user);
            this.Controls.Add(this.email_admin);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.password_admin);
            this.Controls.Add(this.label2);
            this.Name = "login";
            this.Size = new System.Drawing.Size(183, 396);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label errore_login_admin;
        private System.Windows.Forms.Button login_admin;
        private System.Windows.Forms.Button login_user;
        private System.Windows.Forms.TextBox email_admin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox password_admin;
        private System.Windows.Forms.Label label2;
    }
}
