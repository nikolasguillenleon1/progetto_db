﻿
namespace ProgettoDB
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.progetto_DBDataSet = new ProgettoDB.Progetto_DBDataSet();
            this.progettoDBDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.progetto_DBDataSet1 = new ProgettoDB.Progetto_DBDataSet();
            this.label_password = new System.Windows.Forms.Label();
            this.label_admin = new System.Windows.Forms.Label();
            this.errore_login_admin = new System.Windows.Forms.Label();
            this.login_admin = new System.Windows.Forms.Button();
            this.login_user = new System.Windows.Forms.Button();
            this.email_admin = new System.Windows.Forms.TextBox();
            this.label_user = new System.Windows.Forms.Label();
            this.password_admin = new System.Windows.Forms.TextBox();
            this.label_email = new System.Windows.Forms.Label();
            this.logout = new System.Windows.Forms.Button();
            this.to_shop = new System.Windows.Forms.Button();
            this.to_user = new System.Windows.Forms.Button();
            this.to_wishlist = new System.Windows.Forms.Button();
            this.to_carrello = new System.Windows.Forms.Button();
            this.label_benvenuto = new System.Windows.Forms.Label();
            this.modify_products = new System.Windows.Forms.Button();
            this.add_product = new System.Windows.Forms.Button();
            this.remove_product = new System.Windows.Forms.Button();
            this.admin_panel = new System.Windows.Forms.Button();
            this.gestione_indirizzi = new System.Windows.Forms.Button();
            this.gestione_carte = new System.Windows.Forms.Button();
            this.visualizza_p4 = new System.Windows.Forms.Button();
            this.visualizza_p3 = new System.Windows.Forms.Button();
            this.visualizza_p2 = new System.Windows.Forms.Button();
            this.visualizza_p1 = new System.Windows.Forms.Button();
            this.label_negozio = new System.Windows.Forms.Label();
            this.aggiungi_carta2 = new ProgettoDB.aggiungi_carta();
            this.sezione_utente1 = new ProgettoDB.sezione_utente();
            this.dettaglio_prodotto_41 = new ProgettoDB.dettaglio_prodotto_4();
            this.dettaglio_prodotto_31 = new ProgettoDB.dettaglio_prodotto_3();
            this.dettaglio_prodotto_21 = new ProgettoDB.dettaglio_prodotto_2();
            this.dettaglio_prodotto_11 = new ProgettoDB.dettaglio_prodotto_1();
            this.pannello_admin1 = new ProgettoDB.pannello_admin();
            this.wishlist1 = new ProgettoDB.wishlist();
            this.carrello1 = new ProgettoDB.carrello();
            this.rimozione_prodotti1 = new ProgettoDB.rimozione_prodotti();
            this.modifica_prodotti1 = new ProgettoDB.modifica_prodotti();
            this.aggiunta_prodotti1 = new ProgettoDB.aggiunta_prodotti();
            this.aggiungi_indirizzo1 = new ProgettoDB.aggiungi_indirizzo();
            ((System.ComponentModel.ISupportInitialize)(this.progetto_DBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progettoDBDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progetto_DBDataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // progetto_DBDataSet
            // 
            this.progetto_DBDataSet.DataSetName = "Progetto_DBDataSet";
            this.progetto_DBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // progettoDBDataSetBindingSource
            // 
            this.progettoDBDataSetBindingSource.DataSource = this.progetto_DBDataSet;
            this.progettoDBDataSetBindingSource.Position = 0;
            // 
            // progetto_DBDataSet1
            // 
            this.progetto_DBDataSet1.DataSetName = "Progetto_DBDataSet";
            this.progetto_DBDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label_password
            // 
            this.label_password.AutoSize = true;
            this.label_password.Location = new System.Drawing.Point(64, 151);
            this.label_password.Name = "label_password";
            this.label_password.Size = new System.Drawing.Size(53, 13);
            this.label_password.TabIndex = 37;
            this.label_password.Text = "Password";
            // 
            // label_admin
            // 
            this.label_admin.AutoSize = true;
            this.label_admin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_admin.Location = new System.Drawing.Point(55, 84);
            this.label_admin.Name = "label_admin";
            this.label_admin.Size = new System.Drawing.Size(66, 20);
            this.label_admin.TabIndex = 32;
            this.label_admin.Text = "ADMIN";
            // 
            // errore_login_admin
            // 
            this.errore_login_admin.AutoSize = true;
            this.errore_login_admin.Location = new System.Drawing.Point(24, 237);
            this.errore_login_admin.Name = "errore_login_admin";
            this.errore_login_admin.Size = new System.Drawing.Size(145, 13);
            this.errore_login_admin.TabIndex = 36;
            this.errore_login_admin.Text = "Username o password errate.";
            // 
            // login_admin
            // 
            this.login_admin.Location = new System.Drawing.Point(41, 196);
            this.login_admin.Name = "login_admin";
            this.login_admin.Size = new System.Drawing.Size(100, 23);
            this.login_admin.TabIndex = 3;
            this.login_admin.Text = "LOGIN";
            this.login_admin.UseVisualStyleBackColor = true;
            this.login_admin.Click += new System.EventHandler(this.login_admin_Click);
            // 
            // login_user
            // 
            this.login_user.Location = new System.Drawing.Point(41, 300);
            this.login_user.Name = "login_user";
            this.login_user.Size = new System.Drawing.Size(100, 23);
            this.login_user.TabIndex = 35;
            this.login_user.Text = "LOGIN";
            this.login_user.UseVisualStyleBackColor = true;
            this.login_user.Click += new System.EventHandler(this.login_user_Click);
            // 
            // email_admin
            // 
            this.email_admin.Location = new System.Drawing.Point(41, 127);
            this.email_admin.Name = "email_admin";
            this.email_admin.Size = new System.Drawing.Size(100, 20);
            this.email_admin.TabIndex = 1;
            this.email_admin.Text = "admin@admin.com";
            // 
            // label_user
            // 
            this.label_user.AutoSize = true;
            this.label_user.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_user.Location = new System.Drawing.Point(58, 277);
            this.label_user.Name = "label_user";
            this.label_user.Size = new System.Drawing.Size(59, 20);
            this.label_user.TabIndex = 34;
            this.label_user.Text = "USER";
            // 
            // password_admin
            // 
            this.password_admin.Location = new System.Drawing.Point(41, 167);
            this.password_admin.Name = "password_admin";
            this.password_admin.PasswordChar = '*';
            this.password_admin.Size = new System.Drawing.Size(100, 20);
            this.password_admin.TabIndex = 2;
            this.password_admin.Text = "admin";
            // 
            // label_email
            // 
            this.label_email.AutoSize = true;
            this.label_email.Location = new System.Drawing.Point(72, 111);
            this.label_email.Name = "label_email";
            this.label_email.Size = new System.Drawing.Size(32, 13);
            this.label_email.TabIndex = 33;
            this.label_email.Text = "Email";
            // 
            // logout
            // 
            this.logout.Location = new System.Drawing.Point(27, 383);
            this.logout.Name = "logout";
            this.logout.Size = new System.Drawing.Size(143, 23);
            this.logout.TabIndex = 43;
            this.logout.Text = "Logout";
            this.logout.UseVisualStyleBackColor = true;
            this.logout.Click += new System.EventHandler(this.logout_Click);
            // 
            // to_shop
            // 
            this.to_shop.Location = new System.Drawing.Point(27, 72);
            this.to_shop.Name = "to_shop";
            this.to_shop.Size = new System.Drawing.Size(145, 73);
            this.to_shop.TabIndex = 42;
            this.to_shop.Text = "Vai al negozio";
            this.to_shop.UseVisualStyleBackColor = true;
            this.to_shop.Click += new System.EventHandler(this.to_shop_Click);
            // 
            // to_user
            // 
            this.to_user.Location = new System.Drawing.Point(27, 309);
            this.to_user.Name = "to_user";
            this.to_user.Size = new System.Drawing.Size(143, 73);
            this.to_user.TabIndex = 41;
            this.to_user.Text = "Vai alla sezione utente";
            this.to_user.UseVisualStyleBackColor = true;
            this.to_user.Click += new System.EventHandler(this.to_user_Click);
            // 
            // to_wishlist
            // 
            this.to_wishlist.Location = new System.Drawing.Point(27, 230);
            this.to_wishlist.Name = "to_wishlist";
            this.to_wishlist.Size = new System.Drawing.Size(145, 73);
            this.to_wishlist.TabIndex = 40;
            this.to_wishlist.Text = "Vai alla wishlist";
            this.to_wishlist.UseVisualStyleBackColor = true;
            this.to_wishlist.Click += new System.EventHandler(this.to_wishlist_Click);
            // 
            // to_carrello
            // 
            this.to_carrello.Location = new System.Drawing.Point(27, 151);
            this.to_carrello.Name = "to_carrello";
            this.to_carrello.Size = new System.Drawing.Size(145, 73);
            this.to_carrello.TabIndex = 39;
            this.to_carrello.Text = "Vai al carrello";
            this.to_carrello.UseVisualStyleBackColor = true;
            this.to_carrello.Click += new System.EventHandler(this.to_carrello_Click);
            // 
            // label_benvenuto
            // 
            this.label_benvenuto.AutoSize = true;
            this.label_benvenuto.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_benvenuto.Location = new System.Drawing.Point(21, 38);
            this.label_benvenuto.Name = "label_benvenuto";
            this.label_benvenuto.Size = new System.Drawing.Size(162, 31);
            this.label_benvenuto.TabIndex = 38;
            this.label_benvenuto.Text = "Benvenuto!";
            // 
            // modify_products
            // 
            this.modify_products.Location = new System.Drawing.Point(41, 230);
            this.modify_products.Name = "modify_products";
            this.modify_products.Size = new System.Drawing.Size(97, 68);
            this.modify_products.TabIndex = 47;
            this.modify_products.Text = "Modifica prodotti";
            this.modify_products.UseVisualStyleBackColor = true;
            this.modify_products.Click += new System.EventHandler(this.modify_products_Click);
            // 
            // add_product
            // 
            this.add_product.Location = new System.Drawing.Point(41, 82);
            this.add_product.Name = "add_product";
            this.add_product.Size = new System.Drawing.Size(97, 68);
            this.add_product.TabIndex = 46;
            this.add_product.Text = "Aggiungi prodotti";
            this.add_product.UseVisualStyleBackColor = true;
            this.add_product.Click += new System.EventHandler(this.add_product_Click);
            // 
            // remove_product
            // 
            this.remove_product.Location = new System.Drawing.Point(41, 156);
            this.remove_product.Name = "remove_product";
            this.remove_product.Size = new System.Drawing.Size(97, 68);
            this.remove_product.TabIndex = 45;
            this.remove_product.Text = "Rimuovi prodotti";
            this.remove_product.UseVisualStyleBackColor = true;
            this.remove_product.Click += new System.EventHandler(this.remove_product_Click);
            // 
            // admin_panel
            // 
            this.admin_panel.Location = new System.Drawing.Point(41, 304);
            this.admin_panel.Name = "admin_panel";
            this.admin_panel.Size = new System.Drawing.Size(97, 68);
            this.admin_panel.TabIndex = 49;
            this.admin_panel.Text = "Pannello admin";
            this.admin_panel.UseVisualStyleBackColor = true;
            this.admin_panel.Click += new System.EventHandler(this.admin_panel_Click);
            // 
            // gestione_indirizzi
            // 
            this.gestione_indirizzi.Location = new System.Drawing.Point(202, 250);
            this.gestione_indirizzi.Name = "gestione_indirizzi";
            this.gestione_indirizzi.Size = new System.Drawing.Size(119, 35);
            this.gestione_indirizzi.TabIndex = 50;
            this.gestione_indirizzi.Text = "Gestione indirizzi";
            this.gestione_indirizzi.UseVisualStyleBackColor = true;
            this.gestione_indirizzi.Click += new System.EventHandler(this.gestione_indirizzi_Click);
            // 
            // gestione_carte
            // 
            this.gestione_carte.Location = new System.Drawing.Point(202, 291);
            this.gestione_carte.Name = "gestione_carte";
            this.gestione_carte.Size = new System.Drawing.Size(119, 35);
            this.gestione_carte.TabIndex = 51;
            this.gestione_carte.Text = "Gestione carte";
            this.gestione_carte.UseVisualStyleBackColor = true;
            this.gestione_carte.Click += new System.EventHandler(this.gestione_carte_Click);
            // 
            // visualizza_p4
            // 
            this.visualizza_p4.Location = new System.Drawing.Point(604, 153);
            this.visualizza_p4.Name = "visualizza_p4";
            this.visualizza_p4.Size = new System.Drawing.Size(119, 164);
            this.visualizza_p4.TabIndex = 56;
            this.visualizza_p4.Text = "Vai alla pagina di \"Costume Elsa\"";
            this.visualizza_p4.UseVisualStyleBackColor = true;
            this.visualizza_p4.Click += new System.EventHandler(this.visualizza_p4_Click);
            // 
            // visualizza_p3
            // 
            this.visualizza_p3.Location = new System.Drawing.Point(471, 153);
            this.visualizza_p3.Name = "visualizza_p3";
            this.visualizza_p3.Size = new System.Drawing.Size(119, 164);
            this.visualizza_p3.TabIndex = 55;
            this.visualizza_p3.Text = "Vai alla pagina di \"Casco Boba Fett\"";
            this.visualizza_p3.UseVisualStyleBackColor = true;
            this.visualizza_p3.Click += new System.EventHandler(this.visualizza_p3_Click);
            // 
            // visualizza_p2
            // 
            this.visualizza_p2.Location = new System.Drawing.Point(336, 153);
            this.visualizza_p2.Name = "visualizza_p2";
            this.visualizza_p2.Size = new System.Drawing.Size(119, 164);
            this.visualizza_p2.TabIndex = 54;
            this.visualizza_p2.Text = "Vai alla pagina di \"Yoda\"";
            this.visualizza_p2.UseVisualStyleBackColor = true;
            this.visualizza_p2.Click += new System.EventHandler(this.visualizza_p2_Click);
            // 
            // visualizza_p1
            // 
            this.visualizza_p1.Location = new System.Drawing.Point(203, 153);
            this.visualizza_p1.Name = "visualizza_p1";
            this.visualizza_p1.Size = new System.Drawing.Size(119, 164);
            this.visualizza_p1.TabIndex = 53;
            this.visualizza_p1.Text = "Vai alla pagina di \"Cassa Barbie\"";
            this.visualizza_p1.UseVisualStyleBackColor = true;
            this.visualizza_p1.Click += new System.EventHandler(this.visualizza_p1_Click);
            // 
            // label_negozio
            // 
            this.label_negozio.AutoSize = true;
            this.label_negozio.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_negozio.Location = new System.Drawing.Point(383, 38);
            this.label_negozio.Name = "label_negozio";
            this.label_negozio.Size = new System.Drawing.Size(150, 39);
            this.label_negozio.TabIndex = 52;
            this.label_negozio.Text = "Negozio";
            // 
            // aggiungi_carta2
            // 
            this.aggiungi_carta2.Location = new System.Drawing.Point(201, 36);
            this.aggiungi_carta2.Name = "aggiungi_carta2";
            this.aggiungi_carta2.Size = new System.Drawing.Size(545, 370);
            this.aggiungi_carta2.TabIndex = 57;
            // 
            // sezione_utente1
            // 
            this.sezione_utente1.Location = new System.Drawing.Point(191, 38);
            this.sezione_utente1.Name = "sezione_utente1";
            this.sezione_utente1.Size = new System.Drawing.Size(545, 370);
            this.sezione_utente1.TabIndex = 18;
            // 
            // dettaglio_prodotto_41
            // 
            this.dettaglio_prodotto_41.Location = new System.Drawing.Point(191, 38);
            this.dettaglio_prodotto_41.Name = "dettaglio_prodotto_41";
            this.dettaglio_prodotto_41.Size = new System.Drawing.Size(545, 370);
            this.dettaglio_prodotto_41.TabIndex = 17;
            // 
            // dettaglio_prodotto_31
            // 
            this.dettaglio_prodotto_31.Location = new System.Drawing.Point(191, 38);
            this.dettaglio_prodotto_31.Name = "dettaglio_prodotto_31";
            this.dettaglio_prodotto_31.Size = new System.Drawing.Size(545, 370);
            this.dettaglio_prodotto_31.TabIndex = 16;
            // 
            // dettaglio_prodotto_21
            // 
            this.dettaglio_prodotto_21.Location = new System.Drawing.Point(201, 38);
            this.dettaglio_prodotto_21.Name = "dettaglio_prodotto_21";
            this.dettaglio_prodotto_21.Size = new System.Drawing.Size(545, 370);
            this.dettaglio_prodotto_21.TabIndex = 15;
            // 
            // dettaglio_prodotto_11
            // 
            this.dettaglio_prodotto_11.Location = new System.Drawing.Point(191, 38);
            this.dettaglio_prodotto_11.Name = "dettaglio_prodotto_11";
            this.dettaglio_prodotto_11.Size = new System.Drawing.Size(545, 370);
            this.dettaglio_prodotto_11.TabIndex = 14;
            // 
            // pannello_admin1
            // 
            this.pannello_admin1.Location = new System.Drawing.Point(191, 38);
            this.pannello_admin1.Name = "pannello_admin1";
            this.pannello_admin1.Size = new System.Drawing.Size(545, 370);
            this.pannello_admin1.TabIndex = 48;
            // 
            // wishlist1
            // 
            this.wishlist1.Location = new System.Drawing.Point(191, 38);
            this.wishlist1.Name = "wishlist1";
            this.wishlist1.Size = new System.Drawing.Size(545, 370);
            this.wishlist1.TabIndex = 28;
            // 
            // carrello1
            // 
            this.carrello1.Location = new System.Drawing.Point(191, 38);
            this.carrello1.Name = "carrello1";
            this.carrello1.Size = new System.Drawing.Size(545, 370);
            this.carrello1.TabIndex = 27;
            // 
            // rimozione_prodotti1
            // 
            this.rimozione_prodotti1.Location = new System.Drawing.Point(191, 38);
            this.rimozione_prodotti1.Name = "rimozione_prodotti1";
            this.rimozione_prodotti1.Size = new System.Drawing.Size(555, 370);
            this.rimozione_prodotti1.TabIndex = 25;
            // 
            // modifica_prodotti1
            // 
            this.modifica_prodotti1.Location = new System.Drawing.Point(201, 38);
            this.modifica_prodotti1.Name = "modifica_prodotti1";
            this.modifica_prodotti1.Size = new System.Drawing.Size(545, 370);
            this.modifica_prodotti1.TabIndex = 24;
            // 
            // aggiunta_prodotti1
            // 
            this.aggiunta_prodotti1.Location = new System.Drawing.Point(201, 38);
            this.aggiunta_prodotti1.Name = "aggiunta_prodotti1";
            this.aggiunta_prodotti1.Size = new System.Drawing.Size(551, 370);
            this.aggiunta_prodotti1.TabIndex = 23;
            // 
            // aggiungi_indirizzo1
            // 
            this.aggiungi_indirizzo1.Location = new System.Drawing.Point(191, 38);
            this.aggiungi_indirizzo1.Name = "aggiungi_indirizzo1";
            this.aggiungi_indirizzo1.Size = new System.Drawing.Size(545, 370);
            this.aggiungi_indirizzo1.TabIndex = 20;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(776, 413);
            this.Controls.Add(this.to_shop);
            this.Controls.Add(this.add_product);
            this.Controls.Add(this.logout);
            this.Controls.Add(this.label_benvenuto);
            this.Controls.Add(this.label_password);
            this.Controls.Add(this.label_admin);
            this.Controls.Add(this.errore_login_admin);
            this.Controls.Add(this.login_admin);
            this.Controls.Add(this.login_user);
            this.Controls.Add(this.email_admin);
            this.Controls.Add(this.label_user);
            this.Controls.Add(this.password_admin);
            this.Controls.Add(this.label_email);
            this.Controls.Add(this.gestione_indirizzi);
            this.Controls.Add(this.gestione_carte);
            this.Controls.Add(this.visualizza_p2);
            this.Controls.Add(this.visualizza_p3);
            this.Controls.Add(this.visualizza_p4);
            this.Controls.Add(this.to_user);
            this.Controls.Add(this.admin_panel);
            this.Controls.Add(this.to_carrello);
            this.Controls.Add(this.to_wishlist);
            this.Controls.Add(this.modify_products);
            this.Controls.Add(this.remove_product);
            this.Controls.Add(this.visualizza_p1);
            this.Controls.Add(this.aggiungi_carta2);
            this.Controls.Add(this.sezione_utente1);
            this.Controls.Add(this.dettaglio_prodotto_41);
            this.Controls.Add(this.label_negozio);
            this.Controls.Add(this.dettaglio_prodotto_31);
            this.Controls.Add(this.dettaglio_prodotto_21);
            this.Controls.Add(this.dettaglio_prodotto_11);
            this.Controls.Add(this.pannello_admin1);
            this.Controls.Add(this.wishlist1);
            this.Controls.Add(this.carrello1);
            this.Controls.Add(this.rimozione_prodotti1);
            this.Controls.Add(this.modifica_prodotti1);
            this.Controls.Add(this.aggiunta_prodotti1);
            this.Controls.Add(this.aggiungi_indirizzo1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.progetto_DBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progettoDBDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progetto_DBDataSet1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource progettoDBDataSetBindingSource;
        private Progetto_DBDataSet progetto_DBDataSet;
        private Progetto_DBDataSet progetto_DBDataSet1;
        private dettaglio_prodotto_1 dettaglio_prodotto_11;
        private dettaglio_prodotto_2 dettaglio_prodotto_21;
        private dettaglio_prodotto_3 dettaglio_prodotto_31;
        private dettaglio_prodotto_4 dettaglio_prodotto_41;
        private sezione_utente sezione_utente1;
        private aggiungi_indirizzo aggiungi_indirizzo1;
        private aggiunta_prodotti aggiunta_prodotti1;
        private modifica_prodotti modifica_prodotti1;
        private rimozione_prodotti rimozione_prodotti1;
        private carrello carrello1;
        private wishlist wishlist1;
        private System.Windows.Forms.Label label_password;
        private System.Windows.Forms.Label label_admin;
        private System.Windows.Forms.Label errore_login_admin;
        private System.Windows.Forms.Button login_admin;
        private System.Windows.Forms.Button login_user;
        private System.Windows.Forms.TextBox email_admin;
        private System.Windows.Forms.Label label_user;
        private System.Windows.Forms.TextBox password_admin;
        private System.Windows.Forms.Label label_email;
        private System.Windows.Forms.Button logout;
        private System.Windows.Forms.Button to_shop;
        private System.Windows.Forms.Button to_user;
        private System.Windows.Forms.Button to_wishlist;
        private System.Windows.Forms.Button to_carrello;
        private System.Windows.Forms.Label label_benvenuto;
        private System.Windows.Forms.Button modify_products;
        private System.Windows.Forms.Button add_product;
        private System.Windows.Forms.Button remove_product;
        private pannello_admin pannello_admin1;
        private System.Windows.Forms.Button admin_panel;
        private System.Windows.Forms.Button gestione_indirizzi;
        private System.Windows.Forms.Button gestione_carte;
        private System.Windows.Forms.Button visualizza_p4;
        private System.Windows.Forms.Button visualizza_p3;
        private System.Windows.Forms.Button visualizza_p2;
        private System.Windows.Forms.Button visualizza_p1;
        private System.Windows.Forms.Label label_negozio;
        private aggiungi_carta aggiungi_carta2;
    }
}

