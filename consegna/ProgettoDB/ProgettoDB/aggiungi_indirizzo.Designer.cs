﻿
namespace ProgettoDB
{
    partial class aggiungi_indirizzo
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.aggiunta_indirizzo = new System.Windows.Forms.Button();
            this.box_via = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.box_citta = new System.Windows.Forms.TextBox();
            this.box_nazione = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.rimuovi_indirizzo = new System.Windows.Forms.Button();
            this.selettore_indirizzo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.box_cap = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.box_cap)).BeginInit();
            this.SuspendLayout();
            // 
            // aggiunta_indirizzo
            // 
            this.aggiunta_indirizzo.Location = new System.Drawing.Point(3, 198);
            this.aggiunta_indirizzo.Name = "aggiunta_indirizzo";
            this.aggiunta_indirizzo.Size = new System.Drawing.Size(146, 28);
            this.aggiunta_indirizzo.TabIndex = 0;
            this.aggiunta_indirizzo.Text = "Aggiungi indirizzo";
            this.aggiunta_indirizzo.UseVisualStyleBackColor = true;
            this.aggiunta_indirizzo.Click += new System.EventHandler(this.aggiunta_indirizzo_Click);
            // 
            // box_via
            // 
            this.box_via.Location = new System.Drawing.Point(3, 57);
            this.box_via.Name = "box_via";
            this.box_via.Size = new System.Drawing.Size(146, 20);
            this.box_via.TabIndex = 1;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // box_citta
            // 
            this.box_citta.Location = new System.Drawing.Point(3, 94);
            this.box_citta.Name = "box_citta";
            this.box_citta.Size = new System.Drawing.Size(146, 20);
            this.box_citta.TabIndex = 3;
            // 
            // box_nazione
            // 
            this.box_nazione.Location = new System.Drawing.Point(3, 172);
            this.box_nazione.Name = "box_nazione";
            this.box_nazione.Size = new System.Drawing.Size(146, 20);
            this.box_nazione.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Via";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Città";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "CAP";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Nazione";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(155, 41);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(378, 313);
            this.dataGridView1.TabIndex = 10;
            // 
            // rimuovi_indirizzo
            // 
            this.rimuovi_indirizzo.Location = new System.Drawing.Point(3, 271);
            this.rimuovi_indirizzo.Name = "rimuovi_indirizzo";
            this.rimuovi_indirizzo.Size = new System.Drawing.Size(146, 51);
            this.rimuovi_indirizzo.TabIndex = 12;
            this.rimuovi_indirizzo.Text = "Rimuovi questo indirizzo";
            this.rimuovi_indirizzo.UseVisualStyleBackColor = true;
            this.rimuovi_indirizzo.Click += new System.EventHandler(this.rimuovi_indirizzo_Click);
            // 
            // selettore_indirizzo
            // 
            this.selettore_indirizzo.FormattingEnabled = true;
            this.selettore_indirizzo.Location = new System.Drawing.Point(3, 244);
            this.selettore_indirizzo.Name = "selettore_indirizzo";
            this.selettore_indirizzo.Size = new System.Drawing.Size(146, 21);
            this.selettore_indirizzo.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(201, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(177, 25);
            this.label5.TabIndex = 14;
            this.label5.Text = "Gestione indirizzi";
            // 
            // box_cap
            // 
            this.box_cap.Location = new System.Drawing.Point(3, 133);
            this.box_cap.Name = "box_cap";
            this.box_cap.Size = new System.Drawing.Size(146, 20);
            this.box_cap.TabIndex = 3;
            // 
            // aggiungi_indirizzo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.box_cap);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.rimuovi_indirizzo);
            this.Controls.Add(this.selettore_indirizzo);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.box_nazione);
            this.Controls.Add(this.box_citta);
            this.Controls.Add(this.box_via);
            this.Controls.Add(this.aggiunta_indirizzo);
            this.Name = "aggiungi_indirizzo";
            this.Size = new System.Drawing.Size(545, 370);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.box_cap)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button aggiunta_indirizzo;
        private System.Windows.Forms.TextBox box_via;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.TextBox box_citta;
        private System.Windows.Forms.TextBox box_nazione;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button rimuovi_indirizzo;
        private System.Windows.Forms.ComboBox selettore_indirizzo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown box_cap;
    }
}
