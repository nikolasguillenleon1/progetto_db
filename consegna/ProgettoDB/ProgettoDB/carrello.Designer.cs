﻿
namespace ProgettoDB
{
    partial class carrello
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.selettore_prodotto = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.remove_product = new System.Windows.Forms.Button();
            this.svuota_carrello = new System.Windows.Forms.Button();
            this.acquista = new System.Windows.Forms.Button();
            this.refresh = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.selettore_carta = new System.Windows.Forms.ComboBox();
            this.selettore_indirizzo = new System.Windows.Forms.ComboBox();
            this.totale_label = new System.Windows.Forms.Label();
            this.continua = new System.Windows.Forms.Button();
            this.ordine_effettuato_label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(228, 40);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(314, 327);
            this.dataGridView1.TabIndex = 0;
            // selettore_prodotto
            // 
            this.selettore_prodotto.FormattingEnabled = true;
            this.selettore_prodotto.Location = new System.Drawing.Point(3, 40);
            this.selettore_prodotto.Name = "selettore_prodotto";
            this.selettore_prodotto.Size = new System.Drawing.Size(208, 21);
            this.selettore_prodotto.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(242, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Carrello";
            // 
            // remove_product
            // 
            this.remove_product.Location = new System.Drawing.Point(3, 67);
            this.remove_product.Name = "remove_product";
            this.remove_product.Size = new System.Drawing.Size(208, 42);
            this.remove_product.TabIndex = 3;
            this.remove_product.Text = "Rimuovi questo prodotto dal carrello";
            this.remove_product.UseVisualStyleBackColor = true;
            this.remove_product.Click += new System.EventHandler(this.remove_product_Click);
            // 
            // svuota_carrello
            // 
            this.svuota_carrello.Location = new System.Drawing.Point(3, 115);
            this.svuota_carrello.Name = "svuota_carrello";
            this.svuota_carrello.Size = new System.Drawing.Size(208, 42);
            this.svuota_carrello.TabIndex = 5;
            this.svuota_carrello.Text = "Svuota carrello";
            this.svuota_carrello.UseVisualStyleBackColor = true;
            this.svuota_carrello.Click += new System.EventHandler(this.svuota_carrello_Click);
            // 
            // acquista
            // 
            this.acquista.Location = new System.Drawing.Point(3, 333);
            this.acquista.Name = "acquista";
            this.acquista.Size = new System.Drawing.Size(208, 34);
            this.acquista.TabIndex = 6;
            this.acquista.Text = "Acquista";
            this.acquista.UseVisualStyleBackColor = true;
            this.acquista.Click += new System.EventHandler(this.acquista_Click);
            // 
            // refresh
            // 
            this.refresh.Location = new System.Drawing.Point(3, 163);
            this.refresh.Name = "refresh";
            this.refresh.Size = new System.Drawing.Size(208, 23);
            this.refresh.TabIndex = 39;
            this.refresh.Text = "Refresh";
            this.refresh.UseVisualStyleBackColor = true;
            this.refresh.Click += new System.EventHandler(this.refresh_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 247);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 17);
            this.label2.TabIndex = 43;
            this.label2.Text = "Seleziona carta";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 189);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 17);
            this.label3.TabIndex = 42;
            this.label3.Text = "Seleziona indirizzo";
            // 
            // selettore_carta
            // 
            this.selettore_carta.FormattingEnabled = true;
            this.selettore_carta.Location = new System.Drawing.Point(6, 267);
            this.selettore_carta.Name = "selettore_carta";
            this.selettore_carta.Size = new System.Drawing.Size(205, 21);
            this.selettore_carta.TabIndex = 41;
            // 
            // selettore_indirizzo
            // 
            this.selettore_indirizzo.FormattingEnabled = true;
            this.selettore_indirizzo.Location = new System.Drawing.Point(6, 209);
            this.selettore_indirizzo.Name = "selettore_indirizzo";
            this.selettore_indirizzo.Size = new System.Drawing.Size(205, 21);
            this.selettore_indirizzo.TabIndex = 40;
            // 
            // totale_label
            // 
            this.totale_label.AutoSize = true;
            this.totale_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totale_label.Location = new System.Drawing.Point(6, 313);
            this.totale_label.Name = "totale_label";
            this.totale_label.Size = new System.Drawing.Size(59, 17);
            this.totale_label.TabIndex = 44;
            this.totale_label.Text = "Totale:";
            // 
            // continua
            // 
            this.continua.Location = new System.Drawing.Point(166, 191);
            this.continua.Name = "continua";
            this.continua.Size = new System.Drawing.Size(193, 52);
            this.continua.TabIndex = 46;
            this.continua.Text = "Continua";
            this.continua.UseVisualStyleBackColor = true;
            this.continua.Click += new System.EventHandler(this.continua_Click);
            // 
            // ordine_effettuato_label
            // 
            this.ordine_effettuato_label.AutoSize = true;
            this.ordine_effettuato_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ordine_effettuato_label.Location = new System.Drawing.Point(27, 127);
            this.ordine_effettuato_label.Name = "ordine_effettuato_label";
            this.ordine_effettuato_label.Size = new System.Drawing.Size(490, 39);
            this.ordine_effettuato_label.TabIndex = 45;
            this.ordine_effettuato_label.Text = "Ordine avvenuto con successo!";
            // 
            // carrello
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.totale_label);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.selettore_carta);
            this.Controls.Add(this.selettore_indirizzo);
            this.Controls.Add(this.refresh);
            this.Controls.Add(this.acquista);
            this.Controls.Add(this.svuota_carrello);
            this.Controls.Add(this.remove_product);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.selettore_prodotto);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.ordine_effettuato_label);
            this.Controls.Add(this.continua);
            this.Name = "carrello";
            this.Size = new System.Drawing.Size(545, 370);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox selettore_prodotto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button remove_product;
        private System.Windows.Forms.Button svuota_carrello;
        private System.Windows.Forms.Button acquista;
        private System.Windows.Forms.Button refresh;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox selettore_carta;
        private System.Windows.Forms.ComboBox selettore_indirizzo;
        private System.Windows.Forms.Label totale_label;
        private System.Windows.Forms.Button continua;
        private System.Windows.Forms.Label ordine_effettuato_label;
    }
}
