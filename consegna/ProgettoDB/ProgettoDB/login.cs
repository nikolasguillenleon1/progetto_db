﻿using System;
using System.Windows.Forms;

namespace ProgettoDB
{
    public partial class login : UserControl
    {
        public login()
        {
            InitializeComponent();
            errore_login_admin.Hide();
        }

        private void login_admin_Click(object sender, EventArgs e)
        {
            var email = email_admin.Text;
            var password = password_admin.Text;

            if (email.Equals("admin@admin.com") && password.Equals("admin"))
            {
                this.Hide();
            }
            else
            {
                errore_login_admin.Show();
            }
        }
    }
}
