﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace ProgettoDB
{
    public partial class rimozione_prodotti : UserControl
    {
        DataClasse_progettoDBDataContext db = new DataClasse_progettoDBDataContext();
        public rimozione_prodotti()
        {
            InitializeComponent();
            selettore_rimozione.DropDownStyle = ComboBoxStyle.DropDownList;
            selettore_rimozione.DataSource = from p in db.PRODOTTO
                                             select p.IDprodotto;

            dataGridView1.DataSource = from p in db.PRODOTTO
                                       select new { p.IDprodotto, p.nome_prodotto, p.prezzo_unitario, p.quantita_scorta, p.descrizione, p.taglia, p.eta, nome_produttore = p.PRODUTTORE.nome };
        }

        private void rimuovi_prodotto_Click(object sender, EventArgs e)
        {
            var rimozione = (from p in db.PRODOTTO
                             where p.IDprodotto == selettore_rimozione.Text
                             select p);

            if (rimozione != null)
            {
                var app = from a in db.appartenenza
                          where a.IDprodotto == rimozione.FirstOrDefault().IDprodotto
                          select a;

                db.appartenenza.DeleteAllOnSubmit(app);
                db.SubmitChanges();

                db.PRODOTTO.DeleteOnSubmit(rimozione.FirstOrDefault());
                db.SubmitChanges();
                dataGridView1.DataSource = from p in db.PRODOTTO
                                           select new { p.IDprodotto, p.nome_prodotto, p.prezzo_unitario, p.quantita_scorta, p.descrizione, p.taglia, p.eta, nome_produttore = p.PRODUTTORE.nome };
                selettore_rimozione.DataSource = from p in db.PRODOTTO
                                                 select p.IDprodotto;
            }
        }

        private void refresh_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = from p in db.PRODOTTO
                                       select new { p.IDprodotto, p.nome_prodotto, p.prezzo_unitario, p.quantita_scorta, p.descrizione, p.taglia, p.eta, nome_produttore = p.PRODUTTORE.nome };
            selettore_rimozione.DataSource = from p in db.PRODOTTO
                                             select p.IDprodotto;
        }
    }
}
