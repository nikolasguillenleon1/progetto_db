﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace ProgettoDB
{
    public partial class wishlist : UserControl
    {
        DataClasse_progettoDBDataContext db = new DataClasse_progettoDBDataContext();

        public wishlist()
        {
            InitializeComponent();

            var IDwishlist = (from c in db.WISHLIST
                              where c.codice_utente == "000000001"
                              select c.IDwishlist).FirstOrDefault();

            selettore_prodotto.DropDownStyle = ComboBoxStyle.DropDownList;
            selettore_prodotto.DataSource = from cw in db.composizione_wishlist
                                            join p in db.PRODOTTO on cw.IDprodotto equals p.IDprodotto
                                            where cw.IDwishlist == IDwishlist
                                            select cw.IDprodotto;

            dataGridView1.DataSource = from cw in db.composizione_wishlist
                                       join p in db.PRODOTTO on cw.IDprodotto equals p.IDprodotto
                                       where cw.IDwishlist == IDwishlist
                                       select new { cw.PRODOTTO.IDprodotto, cw.PRODOTTO.nome_prodotto, cw.PRODOTTO.descrizione, cw.PRODOTTO.prezzo_unitario };
        }

        private void refresh_Click(object sender, EventArgs e)
        {
            var IDwishlist = (from c in db.WISHLIST
                              where c.codice_utente == "000000001"
                              select c.IDwishlist).FirstOrDefault();

            selettore_prodotto.DropDownStyle = ComboBoxStyle.DropDownList;
            selettore_prodotto.DataSource = from cw in db.composizione_wishlist
                                            join p in db.PRODOTTO on cw.IDprodotto equals p.IDprodotto
                                            where cw.IDwishlist == IDwishlist
                                            select cw.IDprodotto;

            dataGridView1.DataSource = from cw in db.composizione_wishlist
                                       join p in db.PRODOTTO on cw.IDprodotto equals p.IDprodotto
                                       where cw.IDwishlist == IDwishlist
                                       select new { cw.PRODOTTO.IDprodotto, cw.PRODOTTO.nome_prodotto, cw.PRODOTTO.descrizione, cw.PRODOTTO.prezzo_unitario };
        }

        private void remove_product_Click(object sender, EventArgs e)
        {
            var IDwishlist = (from c in db.WISHLIST
                              where c.codice_utente == "000000001"
                              select c.IDwishlist).FirstOrDefault();

            var rimozione = (from cw in db.composizione_wishlist
                             where cw.IDprodotto == selettore_prodotto.Text
                             where cw.IDwishlist == IDwishlist
                             select cw).FirstOrDefault();

            if (rimozione != null)
            {
                db.composizione_wishlist.DeleteOnSubmit(rimozione);
                db.SubmitChanges();
            }

            refresh.PerformClick();
        }

        private void svuota_wishlist_Click(object sender, EventArgs e)
        {
            var IDwishlist = (from c in db.WISHLIST
                              where c.codice_utente == "000000001"
                              select c.IDwishlist).FirstOrDefault();

            var rimozione = from cw in db.composizione_wishlist
                            where cw.IDprodotto == selettore_prodotto.Text
                            where cw.IDwishlist == IDwishlist
                            select cw;

            if (rimozione != null)
            {
                db.composizione_wishlist.DeleteAllOnSubmit(rimozione);
                db.SubmitChanges();
            }

            refresh.PerformClick();
        }
    }
}
