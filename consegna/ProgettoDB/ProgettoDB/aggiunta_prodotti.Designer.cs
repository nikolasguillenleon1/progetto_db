﻿
namespace ProgettoDB
{
    partial class aggiunta_prodotti
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.id_prodotto = new System.Windows.Forms.TextBox();
            this.inserisci_prodotto = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.descrizione = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.taglia = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.scorta = new System.Windows.Forms.Label();
            this.prezzo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.nome = new System.Windows.Forms.TextBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.qta_scorta = new System.Windows.Forms.NumericUpDown();
            this.eta = new System.Windows.Forms.NumericUpDown();
            this.selettore_produttore = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.categoria_1 = new System.Windows.Forms.CheckBox();
            this.categoria_2 = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qta_scorta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eta)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 54;
            this.label3.Text = "ID prodotto";
            // 
            // id_prodotto
            // 
            this.id_prodotto.Location = new System.Drawing.Point(8, 81);
            this.id_prodotto.Name = "id_prodotto";
            this.id_prodotto.Size = new System.Drawing.Size(257, 20);
            this.id_prodotto.TabIndex = 1;
            // 
            // inserisci_prodotto
            // 
            this.inserisci_prodotto.Location = new System.Drawing.Point(8, 323);
            this.inserisci_prodotto.Name = "inserisci_prodotto";
            this.inserisci_prodotto.Size = new System.Drawing.Size(532, 44);
            this.inserisci_prodotto.TabIndex = 9;
            this.inserisci_prodotto.Text = "Inserisci prodotto";
            this.inserisci_prodotto.UseVisualStyleBackColor = true;
            this.inserisci_prodotto.Click += new System.EventHandler(this.inserisci_prodotto_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(157, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(202, 25);
            this.label2.TabIndex = 51;
            this.label2.Text = "Inserimento prodotti";
            // 
            // descrizione
            // 
            this.descrizione.Location = new System.Drawing.Point(8, 243);
            this.descrizione.Name = "descrizione";
            this.descrizione.Size = new System.Drawing.Size(532, 70);
            this.descrizione.TabIndex = 8;
            this.descrizione.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(280, 143);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 13);
            this.label6.TabIndex = 49;
            this.label6.Text = "Età";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(280, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 47;
            this.label5.Text = "Taglia";
            // 
            // taglia
            // 
            this.taglia.Location = new System.Drawing.Point(283, 120);
            this.taglia.Name = "taglia";
            this.taglia.Size = new System.Drawing.Size(257, 20);
            this.taglia.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 227);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 45;
            this.label4.Text = "Descrizione";
            // 
            // scorta
            // 
            this.scorta.AutoSize = true;
            this.scorta.Location = new System.Drawing.Point(280, 65);
            this.scorta.Name = "scorta";
            this.scorta.Size = new System.Drawing.Size(79, 13);
            this.scorta.TabIndex = 44;
            this.scorta.Text = "Quantità scorta";
            // 
            // prezzo
            // 
            this.prezzo.AutoSize = true;
            this.prezzo.Location = new System.Drawing.Point(5, 143);
            this.prezzo.Name = "prezzo";
            this.prezzo.Size = new System.Drawing.Size(76, 13);
            this.prezzo.TabIndex = 42;
            this.prezzo.Text = "Prezzo unitario";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 41;
            this.label1.Text = "Nome prodotto";
            // 
            // nome
            // 
            this.nome.Location = new System.Drawing.Point(8, 120);
            this.nome.Name = "nome";
            this.nome.Size = new System.Drawing.Size(257, 20);
            this.nome.TabIndex = 2;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(8, 159);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(257, 20);
            this.numericUpDown1.TabIndex = 3;
            // 
            // qta_scorta
            // 
            this.qta_scorta.Location = new System.Drawing.Point(283, 82);
            this.qta_scorta.Name = "qta_scorta";
            this.qta_scorta.Size = new System.Drawing.Size(257, 20);
            this.qta_scorta.TabIndex = 4;
            // 
            // eta
            // 
            this.eta.Location = new System.Drawing.Point(283, 159);
            this.eta.Name = "eta";
            this.eta.Size = new System.Drawing.Size(257, 20);
            this.eta.TabIndex = 6;
            // 
            // selettore_produttore
            // 
            this.selettore_produttore.FormattingEnabled = true;
            this.selettore_produttore.Location = new System.Drawing.Point(8, 203);
            this.selettore_produttore.Name = "selettore_produttore";
            this.selettore_produttore.Size = new System.Drawing.Size(257, 21);
            this.selettore_produttore.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 187);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 56;
            this.label7.Text = "Produttore";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(282, 187);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 58;
            this.label8.Text = "Categorie";
            // 
            // categoria_1
            // 
            this.categoria_1.AutoSize = true;
            this.categoria_1.Location = new System.Drawing.Point(285, 207);
            this.categoria_1.Name = "categoria_1";
            this.categoria_1.Size = new System.Drawing.Size(80, 17);
            this.categoria_1.TabIndex = 59;
            this.categoria_1.Text = "checkBox1";
            this.categoria_1.UseVisualStyleBackColor = true;
            // 
            // categoria_2
            // 
            this.categoria_2.AutoSize = true;
            this.categoria_2.Location = new System.Drawing.Point(391, 207);
            this.categoria_2.Name = "categoria_2";
            this.categoria_2.Size = new System.Drawing.Size(80, 17);
            this.categoria_2.TabIndex = 60;
            this.categoria_2.Text = "checkBox2";
            this.categoria_2.UseVisualStyleBackColor = true;
            // 
            // aggiunta_prodotti
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.categoria_2);
            this.Controls.Add(this.categoria_1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.selettore_produttore);
            this.Controls.Add(this.eta);
            this.Controls.Add(this.qta_scorta);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.id_prodotto);
            this.Controls.Add(this.inserisci_prodotto);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.descrizione);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.taglia);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.scorta);
            this.Controls.Add(this.prezzo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nome);
            this.Name = "aggiunta_prodotti";
            this.Size = new System.Drawing.Size(545, 370);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qta_scorta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eta)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox id_prodotto;
        private System.Windows.Forms.Button inserisci_prodotto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox descrizione;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox taglia;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label scorta;
        private System.Windows.Forms.Label prezzo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nome;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.NumericUpDown qta_scorta;
        private System.Windows.Forms.NumericUpDown eta;
        private System.Windows.Forms.ComboBox selettore_produttore;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox categoria_1;
        private System.Windows.Forms.CheckBox categoria_2;
    }
}
