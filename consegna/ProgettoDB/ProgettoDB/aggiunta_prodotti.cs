﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace ProgettoDB
{
    public partial class aggiunta_prodotti : UserControl
    {
        DataClasse_progettoDBDataContext db = new DataClasse_progettoDBDataContext();
        public aggiunta_prodotti()
        {
            InitializeComponent();
            selettore_produttore.DropDownStyle = ComboBoxStyle.DropDownList;
            selettore_produttore.DataSource = from p in db.PRODUTTORE
                                              select p.nome;

            var categorie = from c in db.CATEGORIA
                            select c.nome;

            var index = 0;
            foreach (var c in categorie)
            {
                if(index == 0)
                {
                    categoria_1.Text = c;
                }
                else
                {
                    categoria_2.Text = c;
                }
                index++;
            }
        }

        private void inserisci_prodotto_Click(object sender, EventArgs e)
        {
            var produttore = from p in db.PRODUTTORE
                             where p.nome == selettore_produttore.Text
                             select p;

            if (categoria_1.Checked)
            {
                var cat_1 = from c in db.CATEGORIA
                            where c.nome == categoria_1.Text
                            select c.IDcategoria;

                appartenenza appartenenza_categoria = new appartenenza
                {
                    IDcategoria = cat_1.FirstOrDefault(),
                    IDprodotto = id_prodotto.Text,
                };

                db.appartenenza.InsertOnSubmit(appartenenza_categoria);
            }

            if(categoria_2.Checked)
            {
                var cat_2 = from c in db.CATEGORIA
                            where c.nome == categoria_2.Text
                            select c.IDcategoria;

                appartenenza appartenenza_categoria = new appartenenza
                {
                    IDcategoria = cat_2.FirstOrDefault(),
                    IDprodotto = id_prodotto.Text,
                };

                db.appartenenza.InsertOnSubmit(appartenenza_categoria);
            }


            PRODOTTO prod = new PRODOTTO
            {
                IDprodotto = id_prodotto.Text,
                nome_prodotto = nome.Text,
                prezzo_unitario = numericUpDown1.Value,
                quantita_scorta = qta_scorta.Value,
                taglia = taglia.Text,
                eta = eta.Value,
                descrizione = descrizione.Text,
                PRODUTTORE = produttore.FirstOrDefault(),
            };

            

            db.PRODOTTO.InsertOnSubmit(prod);
            db.SubmitChanges();
            id_prodotto.Clear();
            nome.Clear();
            numericUpDown1.ResetText();
            qta_scorta.ResetText();
            taglia.Clear();
            eta.ResetText();
            descrizione.Clear();
        }
    }
}
