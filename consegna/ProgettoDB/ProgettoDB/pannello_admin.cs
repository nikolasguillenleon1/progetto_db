﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace ProgettoDB
{
    public partial class pannello_admin : UserControl
    {

        DataClasse_progettoDBDataContext db = new DataClasse_progettoDBDataContext();
        public pannello_admin()
        {
            InitializeComponent();
            selettore_produttore.DropDownStyle = ComboBoxStyle.DropDownList;
            selettore_produttore.DataSource = from p in db.PRODUTTORE
                                              select p.nome;
            selettore_categoria.DropDownStyle = ComboBoxStyle.DropDownList;
            selettore_categoria.DataSource = from p in db.CATEGORIA
                                             select p.nome;
            selettore_mese.DropDownStyle = ComboBoxStyle.DropDownList;
            selettore_mese.DataSource = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat
                                        .MonthNames.Take(12).ToList();
        }

        private void pannello_admin_Load(object sender, EventArgs e)
        {

        }

        private void filtro_produttore_Click(object sender, EventArgs e)
        {
            var produttore = (String)selettore_produttore.SelectedItem;
            dataGridView1.DataSource = from prod in db.PRODOTTO
                                       where prod.PRODUTTORE.nome == produttore
                                       select new { prod.IDprodotto, prod.nome_prodotto, nome_produttore = prod.PRODUTTORE.nome, prod.prezzo_unitario, prod.quantita_scorta, prod.descrizione, prod.taglia, prod.eta };
        }

        private void filtro_categoria_Click(object sender, EventArgs e)
        {
            var categoria = (String)selettore_categoria.SelectedItem;
            dataGridView1.DataSource = from p in db.PRODOTTO
                                       from a in db.appartenenza
                                       from c in db.CATEGORIA
                                       where c.nome == categoria && a.IDcategoria == c.IDcategoria && p.IDprodotto == a.IDprodotto
                                       select new { p.nome_prodotto, nome_categoria = c.nome, p.IDprodotto, p.IDproduttore, produttore = p.PRODUTTORE.nome, p.prezzo_unitario, p.quantita_scorta, p.descrizione, p.taglia, p.eta };


        }
        

        private void ricavato_mensile_Click(object sender, EventArgs e)
        {
            var mesi = new Dictionary<string, int>()
            {
                {"January", 1 },
                {"February", 2},
                {"March", 3},
                {"April", 4},
                {"May", 5},
                {"June", 6},
                {"July", 7},
                {"August", 8},
                {"September", 9},
                {"October", 10},
                {"November", 11},
                {"December", 12},
            };

            dataGridView1.DataSource = from o in db.ORDINE
                                       where o.data_ordine.Month == mesi[selettore_mese.Text]
                                       group o by 1 into g
                                       select new { ricavato_mensile = g.Sum(x => x.importo) };
        }

        private void top_5_venduti_Click(object sender, EventArgs e)
        {
            var prodotto_quantita = from o in db.DETTAGLIO_ORDINE
                                    group o by o.IDprodotto into g
                                    select new
                                    {
                                        IDprodotto = g.Key,
                                        Quantita_totale_venduto = g.Sum(x => x.quantita)
                                    };

            var top_5 = (from p in db.PRODOTTO
                         join q in prodotto_quantita on p.IDprodotto equals q.IDprodotto
                         select new
                         {
                             IDprodotto = p.IDprodotto,
                             Nome_prodotto = p.nome_prodotto,
                             Ricavato = p.prezzo_unitario * q.Quantita_totale_venduto
                         }).OrderByDescending(x => x.Ricavato).Take(5);

            dataGridView1.DataSource = top_5;
        }

        private void elenco_prodotti_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = from p in db.PRODOTTO
                                       select new { p.IDprodotto, p.nome_prodotto, p.prezzo_unitario, p.quantita_scorta, p.descrizione, p.taglia, p.eta, nome_produttore = p.PRODUTTORE.nome };
        }
    }
}
