﻿
namespace ProgettoDB
{
    partial class sezione_utente
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.rimuovi_filtri = new System.Windows.Forms.Button();
            this.data_inizio = new System.Windows.Forms.DateTimePicker();
            this.data_fine = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.visualizza_ordini = new System.Windows.Forms.Button();
            this.visualizza_notifiche = new System.Windows.Forms.Button();
            this.visualizza_recensioni = new System.Windows.Forms.Button();
            this.visualizza_fatture = new System.Windows.Forms.Button();
            this.filtra_ordini = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(191, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 25);
            this.label2.TabIndex = 21;
            this.label2.Text = "Sezione Utente";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(150, 48);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(381, 253);
            this.dataGridView1.TabIndex = 23;
            // 
            // rimuovi_filtri
            // 
            this.rimuovi_filtri.Location = new System.Drawing.Point(11, 330);
            this.rimuovi_filtri.Name = "rimuovi_filtri";
            this.rimuovi_filtri.Size = new System.Drawing.Size(119, 29);
            this.rimuovi_filtri.TabIndex = 27;
            this.rimuovi_filtri.Text = "Rimuovi filtri ordini";
            this.rimuovi_filtri.UseVisualStyleBackColor = true;
            this.rimuovi_filtri.Click += new System.EventHandler(this.rimuovi_filtri_Click);
            // 
            // data_inizio
            // 
            this.data_inizio.Location = new System.Drawing.Point(150, 339);
            this.data_inizio.Name = "data_inizio";
            this.data_inizio.Size = new System.Drawing.Size(164, 20);
            this.data_inizio.TabIndex = 28;
            // 
            // data_fine
            // 
            this.data_fine.Location = new System.Drawing.Point(355, 339);
            this.data_fine.Name = "data_fine";
            this.data_fine.Size = new System.Drawing.Size(161, 20);
            this.data_fine.TabIndex = 29;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(147, 307);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(157, 17);
            this.label3.TabIndex = 30;
            this.label3.Text = "Filtra ordini per data";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(147, 324);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 13);
            this.label4.TabIndex = 31;
            this.label4.Text = "Da";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(352, 323);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "A";
            // 
            // visualizza_ordini
            // 
            this.visualizza_ordini.Location = new System.Drawing.Point(11, 48);
            this.visualizza_ordini.Name = "visualizza_ordini";
            this.visualizza_ordini.Size = new System.Drawing.Size(119, 35);
            this.visualizza_ordini.TabIndex = 36;
            this.visualizza_ordini.Text = "Visualizza ordini";
            this.visualizza_ordini.UseVisualStyleBackColor = true;
            this.visualizza_ordini.Click += new System.EventHandler(this.visualizza_ordini_Click);
            // 
            // visualizza_notifiche
            // 
            this.visualizza_notifiche.Location = new System.Drawing.Point(11, 128);
            this.visualizza_notifiche.Name = "visualizza_notifiche";
            this.visualizza_notifiche.Size = new System.Drawing.Size(119, 36);
            this.visualizza_notifiche.TabIndex = 37;
            this.visualizza_notifiche.Text = "Visualizza notifiche";
            this.visualizza_notifiche.UseVisualStyleBackColor = true;
            this.visualizza_notifiche.Click += new System.EventHandler(this.visualizza_notifiche_Click);
            // 
            // visualizza_recensioni
            // 
            this.visualizza_recensioni.Location = new System.Drawing.Point(11, 170);
            this.visualizza_recensioni.Name = "visualizza_recensioni";
            this.visualizza_recensioni.Size = new System.Drawing.Size(119, 36);
            this.visualizza_recensioni.TabIndex = 38;
            this.visualizza_recensioni.Text = "Visualizza le mie recensioni";
            this.visualizza_recensioni.UseVisualStyleBackColor = true;
            this.visualizza_recensioni.Click += new System.EventHandler(this.visualizza_recensioni_Click);
            // 
            // visualizza_fatture
            // 
            this.visualizza_fatture.Location = new System.Drawing.Point(11, 89);
            this.visualizza_fatture.Name = "visualizza_fatture";
            this.visualizza_fatture.Size = new System.Drawing.Size(119, 33);
            this.visualizza_fatture.TabIndex = 35;
            this.visualizza_fatture.Text = "Visualizza fatture";
            this.visualizza_fatture.UseVisualStyleBackColor = true;
            this.visualizza_fatture.Click += new System.EventHandler(this.visualizza_fatture_Click);
            // 
            // filtra_ordini
            // 
            this.filtra_ordini.Location = new System.Drawing.Point(11, 295);
            this.filtra_ordini.Name = "filtra_ordini";
            this.filtra_ordini.Size = new System.Drawing.Size(119, 29);
            this.filtra_ordini.TabIndex = 39;
            this.filtra_ordini.Text = "Filtra ordini";
            this.filtra_ordini.UseVisualStyleBackColor = true;
            this.filtra_ordini.Click += new System.EventHandler(this.filtra_ordini_Click);
            // 
            // sezione_utente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.filtra_ordini);
            this.Controls.Add(this.visualizza_recensioni);
            this.Controls.Add(this.visualizza_notifiche);
            this.Controls.Add(this.visualizza_ordini);
            this.Controls.Add(this.visualizza_fatture);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.data_fine);
            this.Controls.Add(this.data_inizio);
            this.Controls.Add(this.rimuovi_filtri);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label2);
            this.Name = "sezione_utente";
            this.Size = new System.Drawing.Size(545, 370);
            this.Load += new System.EventHandler(this.sezione_utente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button rimuovi_filtri;
        private System.Windows.Forms.DateTimePicker data_inizio;
        private System.Windows.Forms.DateTimePicker data_fine;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button visualizza_ordini;
        private System.Windows.Forms.Button visualizza_notifiche;
        private System.Windows.Forms.Button visualizza_recensioni;
        private System.Windows.Forms.Button visualizza_fatture;
        private System.Windows.Forms.Button filtra_ordini;
    }
}
