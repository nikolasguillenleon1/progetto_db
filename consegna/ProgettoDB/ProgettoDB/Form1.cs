﻿using System;
using System.Windows.Forms;

namespace ProgettoDB
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            hide_all();
            gestione_carte.Hide();
            gestione_indirizzi.Hide();
            label_benvenuto.Hide();
            to_shop.Hide();
            to_carrello.Hide();
            to_wishlist.Hide();
            to_user.Hide();
            logout.Hide();
            add_product.Hide();
            remove_product.Hide();
            modify_products.Hide();
            admin_panel.Hide();
            hide_shop();
            label_admin.Show();
            label_email.Show();
            email_admin.Show();
            label_password.Show();
            password_admin.Show();
            login_admin.Show();
            errore_login_admin.Hide();
            label_user.Show();
            login_user.Show();
        }

        //Hides all the User Controls
        public void hide_all()
        {
            aggiungi_carta2.Hide();
            aggiungi_indirizzo1.Hide();
            aggiunta_prodotti1.Hide();
            carrello1.Hide();
            dettaglio_prodotto_11.Hide();
            dettaglio_prodotto_21.Hide();
            dettaglio_prodotto_31.Hide();
            dettaglio_prodotto_41.Hide();
            modifica_prodotti1.Hide();
            rimozione_prodotti1.Hide();
            sezione_utente1.Hide();
            sezione_utente1.Hide();
            wishlist1.Hide();
            hide_shop();
            pannello_admin1.Hide();
        }

        // Hides login and shows User Sidebar
        public void show_user_sidebar()
        {
            // Hide login
            label_admin.Hide();
            label_email.Hide();
            email_admin.Hide();
            label_password.Hide();
            password_admin.Hide();
            login_admin.Hide();
            errore_login_admin.Hide();
            label_user.Hide();
            login_user.Hide();

            // Hide Admin Sidebar
            add_product.Hide();
            remove_product.Hide();
            modify_products.Hide();
            admin_panel.Hide();
            gestione_indirizzi.Hide();
            gestione_carte.Hide();

            // Show Sidebar
            label_benvenuto.Show();
            to_shop.Show();
            to_carrello.Show();
            to_wishlist.Show();
            to_user.Show();
            logout.Show();

            // Show shop
            show_shop();
        }

        // Goes back to login page
        public void show_login_form()
        {
            // Hide Sidebar
            label_benvenuto.Hide();
            to_shop.Hide();
            to_carrello.Hide();
            to_wishlist.Hide();
            to_user.Hide();
            logout.Hide();
            add_product.Hide();
            modify_products.Hide();
            remove_product.Hide();
            admin_panel.Hide();
            gestione_carte.Hide();
            gestione_indirizzi.Hide();

            //Hide all the other User Controls
            hide_all();

            // Show login
            label_admin.Show();
            label_email.Show();
            email_admin.Show();
            label_password.Show();
            password_admin.Show();
            login_admin.Show();
            errore_login_admin.Hide();
            label_user.Show();
            login_user.Show();
        }

        private void show_shop()
        {
            label_negozio.Show();
            visualizza_p1.Show();
            visualizza_p2.Show();
            visualizza_p3.Show();
            visualizza_p4.Show();
        }

        private void hide_shop()
        {
            label_negozio.Hide();
            visualizza_p1.Hide();
            visualizza_p2.Hide();
            visualizza_p3.Hide();
            visualizza_p4.Hide();
        }

        private void login_admin_Click(object sender, EventArgs e)
        {
            var email = email_admin.Text;
            var password = password_admin.Text;

            if (email.Equals("admin@admin.com") && password.Equals("admin"))
            {
                hide_all();
                label_admin.Hide();
                label_email.Hide();
                email_admin.Hide();
                label_password.Hide();
                password_admin.Hide();
                login_admin.Hide();
                errore_login_admin.Hide();
                label_user.Hide();
                login_user.Hide();
                gestione_indirizzi.Hide();
                gestione_carte.Hide();
                pannello_admin1.Show();
                add_product.Show();
                modify_products.Show();
                remove_product.Show();
                admin_panel.Show();

                // Logout button is universal, it does not depend on the current panel
                logout.Show();

            }
            else
            {
                errore_login_admin.Show();
            }
        }

        private void login_user_Click(object sender, EventArgs e)
        {
            show_user_sidebar();
            hide_all();
            label_negozio.Show();
            visualizza_p1.Show();
            visualizza_p2.Show();
            visualizza_p3.Show();
            visualizza_p4.Show();
        }

        private void logout_Click(object sender, EventArgs e)
        {
            hide_all();
            logout.Hide();
            show_login_form();
        }

        private void add_product_Click(object sender, EventArgs e)
        {
            hide_all();
            aggiunta_prodotti1.Show();
        }

        private void remove_product_Click(object sender, EventArgs e)
        {
            hide_all();
            rimozione_prodotti1.Show();
        }

        private void modify_products_Click(object sender, EventArgs e)
        {
            hide_all();
            modifica_prodotti1.Show();
        }

        private void admin_panel_Click(object sender, EventArgs e)
        {
            hide_all();
            pannello_admin1.Show();
        }

        private void to_shop_Click(object sender, EventArgs e)
        {
            hide_all();
            gestione_indirizzi.Hide();
            gestione_carte.Hide();
            show_shop();
        }

        private void to_carrello_Click(object sender, EventArgs e)
        {
            hide_all();
            gestione_carte.Hide();
            gestione_indirizzi.Hide();
            carrello1.Show();
        }

        private void to_wishlist_Click(object sender, EventArgs e)
        {
            hide_all();
            gestione_carte.Hide();
            gestione_indirizzi.Hide();
            wishlist1.Show();
        }

        private void to_user_Click(object sender, EventArgs e)
        {
            hide_all();
            sezione_utente1.Show();
            gestione_indirizzi.Show();
            gestione_carte.Show();
            gestione_indirizzi.BringToFront();
            gestione_carte.BringToFront();
        }

        private void gestione_indirizzi_Click(object sender, EventArgs e)
        {
            hide_all();
            gestione_carte.Hide();
            gestione_indirizzi.Hide();
            aggiungi_indirizzo1.Show();
        }

        private void gestione_carte_Click(object sender, EventArgs e)
        {
            hide_all();
            gestione_carte.Hide();
            gestione_indirizzi.Hide();
            aggiungi_carta2.Show();
        }

        private void visualizza_p1_Click(object sender, EventArgs e)
        {
            hide_all();
            gestione_carte.Hide();
            gestione_indirizzi.Hide();
            dettaglio_prodotto_11.Show();
        }

        private void visualizza_p2_Click(object sender, EventArgs e)
        {
            hide_all();
            gestione_carte.Hide();
            gestione_indirizzi.Hide();
            dettaglio_prodotto_21.Show();
        }

        private void visualizza_p3_Click(object sender, EventArgs e)
        {
            hide_all();
            gestione_carte.Hide();
            gestione_indirizzi.Hide();
            dettaglio_prodotto_31.Show();
        }

        private void visualizza_p4_Click(object sender, EventArgs e)
        {
            hide_all();
            gestione_carte.Hide();
            gestione_indirizzi.Hide();
            dettaglio_prodotto_41.Show();
        }
    }
}
