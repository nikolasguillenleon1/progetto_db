﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace ProgettoDB
{
    public partial class carrello : UserControl
    {
        DataClasse_progettoDBDataContext db = new DataClasse_progettoDBDataContext();
        Random rnd = new Random();

        public carrello()
        {
            InitializeComponent();
            ordine_effettuato_label.Hide();
            continua.Hide();
            var IDcarrello = (from c in db.CARRELLO
                              where c.codice_utente == "000000001"
                              select c.IDcarrello).FirstOrDefault();

            selettore_prodotto.DropDownStyle = ComboBoxStyle.DropDownList;
            selettore_prodotto.DataSource = from cc in db.composizione_carrello
                                            join p in db.PRODOTTO on cc.IDprodotto equals p.IDprodotto
                                            where cc.IDcarrello == IDcarrello
                                            select cc.IDprodotto;

            dataGridView1.DataSource = from cc in db.composizione_carrello
                                       join p in db.PRODOTTO on cc.IDprodotto equals p.IDprodotto
                                       where cc.IDcarrello == IDcarrello
                                       select new { cc.PRODOTTO.IDprodotto, cc.PRODOTTO.nome_prodotto, cc.PRODOTTO.descrizione, cc.PRODOTTO.prezzo_unitario };

            selettore_indirizzo.DropDownStyle = ComboBoxStyle.DropDownList;
            selettore_indirizzo.DataSource = from i in db.INDIRIZZO
                                             join g in db.intestazione_indirizzo on i.IDindirizzo equals g.IDindirizzo
                                             where g.UTENTE.codice == "000000001"
                                             select new { indirizzo = g.INDIRIZZO.via + ", " + g.INDIRIZZO.citta + ", " + g.INDIRIZZO.CAP + ", " + g.INDIRIZZO.nazione };

            selettore_carta.DropDownStyle = ComboBoxStyle.DropDownList;
            selettore_carta.DataSource = from i in db.CARTA_DI_CREDITO
                                         join g in db.intestazione_carta on i.numero_carta equals g.numero_carta
                                         where g.UTENTE.codice == "000000001"
                                         select new { carta = g.CARTA_DI_CREDITO.numero_carta };

            refresh_totale();
        }

        private void refresh_totale()
        {
            var IDcarrello = (from c in db.CARRELLO
                              where c.codice_utente == "000000001"
                              select c.IDcarrello).FirstOrDefault();

            totale_label.Text = "Totale: " + (from c in db.CARRELLO
                                              where c.IDcarrello == IDcarrello
                                              select c.totale).FirstOrDefault().ToString() + " €";
        }

        private void refresh_Click(object sender, EventArgs e)
        {
            var IDcarrello = (from c in db.CARRELLO
                              where c.codice_utente == "000000001"
                              select c.IDcarrello).FirstOrDefault();

            dataGridView1.DataSource = from cc in db.composizione_carrello
                                       join p in db.PRODOTTO on cc.IDprodotto equals p.IDprodotto
                                       where cc.IDcarrello == IDcarrello
                                       select new { cc.PRODOTTO.IDprodotto, cc.PRODOTTO.nome_prodotto, cc.PRODOTTO.descrizione, cc.PRODOTTO.prezzo_unitario, cc.quantita };

            selettore_prodotto.DataSource = from cc in db.composizione_carrello
                                            join p in db.PRODOTTO on cc.IDprodotto equals p.IDprodotto
                                            where cc.IDcarrello == IDcarrello
                                            select cc.IDprodotto;

            selettore_indirizzo.DataSource = from i in db.INDIRIZZO
                                             join g in db.intestazione_indirizzo on i.IDindirizzo equals g.IDindirizzo
                                             where g.UTENTE.codice == "000000001"
                                             select new { indirizzo = g.INDIRIZZO.via + ", " + g.INDIRIZZO.citta + ", " + g.INDIRIZZO.CAP + ", " + g.INDIRIZZO.nazione };

            selettore_carta.DataSource = from i in db.CARTA_DI_CREDITO
                                         join g in db.intestazione_carta on i.numero_carta equals g.numero_carta
                                         where g.UTENTE.codice == "000000001"
                                         select new { carta = g.CARTA_DI_CREDITO.numero_carta };

            refresh_totale();
        }

        private void remove_product_Click(object sender, EventArgs e)
        {

            var IDcarrello = (from c in db.CARRELLO
                              where c.codice_utente == "000000001"
                              select c.IDcarrello).FirstOrDefault();

            var rimozione = (from cc in db.composizione_carrello
                             where cc.IDprodotto == selettore_prodotto.Text
                             where cc.IDcarrello == IDcarrello
                             select cc).FirstOrDefault();

            if (rimozione != null)
            {
                db.composizione_carrello.DeleteOnSubmit(rimozione);
                db.SubmitChanges();
            }

            var carrello = (from c in db.CARRELLO
                            where c.codice_utente == "000000001"
                            select c).FirstOrDefault();

            carrello.totale = carrello.totale - (from p in db.PRODOTTO
                                                 where p.IDprodotto == selettore_prodotto.Text
                                                 select p.prezzo_unitario).FirstOrDefault();

            db.SubmitChanges();
            refresh_totale();
            refresh.PerformClick();
        }

        private void svuota_carrello_Click(object sender, EventArgs e)
        {
            var IDcarrello = (from c in db.CARRELLO
                              where c.codice_utente == "000000001"
                              select c.IDcarrello).FirstOrDefault();

            var rimozione = from cc in db.composizione_carrello
                            where cc.IDcarrello == IDcarrello
                            select cc;

            if (rimozione != null)
            {
                db.composizione_carrello.DeleteAllOnSubmit(rimozione);
                db.SubmitChanges();
            }

            var carrello = (from c in db.CARRELLO
                            where c.codice_utente == "000000001"
                            select c).FirstOrDefault();

            carrello.totale = 0;

            db.SubmitChanges();
            refresh.PerformClick();
        }

        private void acquista_Click(object sender, EventArgs e)
        {
            selettore_prodotto.Hide();
            remove_product.Hide();
            svuota_carrello.Hide();
            refresh.Hide();
            label3.Hide();
            selettore_indirizzo.Hide();
            label2.Hide();
            selettore_carta.Hide();
            totale_label.Hide();
            acquista.Hide();
            dataGridView1.Hide();

            var IDcarrello = (from c in db.CARRELLO
                              where c.codice_utente == "000000001"
                              select c.IDcarrello).FirstOrDefault();

            ORDINE ordine = new ORDINE
            {
                IDordine = rnd.Next(1, 99999).ToString(),
                importo = (from c in db.CARRELLO
                           where c.codice_utente == "000000001"
                           select c.totale).FirstOrDefault(),

                email_ordine = (from u in db.UTENTE
                                where u.codice == "000000001"
                                select u.email).FirstOrDefault(),

                data_ordine = DateTime.Today,
            };

            db.ORDINE.InsertOnSubmit(ordine);

            var prodotti_ordine = from p in db.composizione_carrello
                                  where p.IDcarrello == IDcarrello
                                  select p;

            var numero_linea = 1;

            foreach (var riga in prodotti_ordine)
            {
                DETTAGLIO_ORDINE dettaglio_ordine = new DETTAGLIO_ORDINE
                {
                    numero_linea = numero_linea,
                    IDordine = ordine.IDordine,
                    IDprodotto = riga.IDprodotto,
                    quantita = riga.quantita,
                };
                numero_linea = numero_linea + 1;

                db.DETTAGLIO_ORDINE.InsertOnSubmit(dettaglio_ordine);

                var prodotto = (from p in db.PRODOTTO
                                where p.IDprodotto == riga.IDprodotto
                                select p).FirstOrDefault();

                prodotto.quantita_scorta = prodotto.quantita_scorta - riga.quantita;

            }

            FATTURA fattura = new FATTURA
            {
                codice_fattura = rnd.Next(1, 999999).ToString(),
                IDordine = ordine.IDordine,
                data_fattura = ordine.data_ordine,
                importo_netto = ordine.importo - (int)((ordine.importo / 100) * 22),
                IVA = 22,
                codice_utente = "000000001",
            };

            db.FATTURA.InsertOnSubmit(fattura);

            foreach (var p in prodotti_ordine)
            {
                db.composizione_carrello.DeleteOnSubmit(p);
            }

            db.SubmitChanges();

            svuota_carrello.PerformClick();
            ordine_effettuato_label.Show();
            continua.Show();

        }

        private void continua_Click(object sender, EventArgs e)
        {
            ordine_effettuato_label.Hide();
            continua.Hide();
            selettore_prodotto.Show();
            remove_product.Show();
            svuota_carrello.Show();
            refresh.Show();
            label3.Show();
            selettore_indirizzo.Show();
            label2.Show();
            selettore_carta.Show();
            totale_label.Show();
            acquista.Show();
            dataGridView1.Show();

            svuota_carrello.PerformClick();
        }
    }
}
