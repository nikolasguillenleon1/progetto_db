﻿
namespace ProgettoDB
{
    partial class dettaglio_prodotto_4
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.recensioni_grid = new System.Windows.Forms.DataGridView();
            this.aggiunta_wishlist = new System.Windows.Forms.Button();
            this.aggiunta_carrello = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.aggiorna_recensioni = new System.Windows.Forms.Button();
            this.punteggio_input = new System.Windows.Forms.NumericUpDown();
            this.titolo_box = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pubblica_recensione = new System.Windows.Forms.Button();
            this.recensione_box = new System.Windows.Forms.RichTextBox();
            this.quantita_input = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.recensioni_grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.punteggio_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantita_input)).BeginInit();
            this.SuspendLayout();
            // 
            // recensioni_grid
            // 
            this.recensioni_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.recensioni_grid.Location = new System.Drawing.Point(139, 172);
            this.recensioni_grid.Name = "recensioni_grid";
            this.recensioni_grid.Size = new System.Drawing.Size(392, 187);
            this.recensioni_grid.TabIndex = 25;
            // 
            // aggiunta_wishlist
            // 
            this.aggiunta_wishlist.Location = new System.Drawing.Point(14, 137);
            this.aggiunta_wishlist.Name = "aggiunta_wishlist";
            this.aggiunta_wishlist.Size = new System.Drawing.Size(119, 52);
            this.aggiunta_wishlist.TabIndex = 22;
            this.aggiunta_wishlist.Text = "Aggiungi Costume Elsa alla wishlist";
            this.aggiunta_wishlist.UseVisualStyleBackColor = true;
            this.aggiunta_wishlist.Click += new System.EventHandler(this.aggiunta_wishlist_Click);
            // 
            // aggiunta_carrello
            // 
            this.aggiunta_carrello.Location = new System.Drawing.Point(14, 56);
            this.aggiunta_carrello.Name = "aggiunta_carrello";
            this.aggiunta_carrello.Size = new System.Drawing.Size(119, 52);
            this.aggiunta_carrello.TabIndex = 21;
            this.aggiunta_carrello.Text = "Aggiungi  Costume Elsa al carrello";
            this.aggiunta_carrello.UseVisualStyleBackColor = true;
            this.aggiunta_carrello.Click += new System.EventHandler(this.aggiunta_carrello_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(219, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 25);
            this.label1.TabIndex = 20;
            this.label1.Text = "Costume Elsa";
            // 
            // aggiorna_recensioni
            // 
            this.aggiorna_recensioni.Location = new System.Drawing.Point(14, 307);
            this.aggiorna_recensioni.Name = "aggiorna_recensioni";
            this.aggiorna_recensioni.Size = new System.Drawing.Size(119, 52);
            this.aggiorna_recensioni.TabIndex = 26;
            this.aggiorna_recensioni.Text = "Aggiorna recensioni";
            this.aggiorna_recensioni.UseVisualStyleBackColor = true;
            this.aggiorna_recensioni.Click += new System.EventHandler(this.aggiorna_recensioni_Click);
            // 
            // punteggio_input
            // 
            this.punteggio_input.Location = new System.Drawing.Point(483, 56);
            this.punteggio_input.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.punteggio_input.Name = "punteggio_input";
            this.punteggio_input.Size = new System.Drawing.Size(48, 20);
            this.punteggio_input.TabIndex = 32;
            // 
            // titolo_box
            // 
            this.titolo_box.Location = new System.Drawing.Point(171, 58);
            this.titolo_box.Name = "titolo_box";
            this.titolo_box.Size = new System.Drawing.Size(245, 20);
            this.titolo_box.TabIndex = 31;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(422, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "Punteggio";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(139, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 29;
            this.label2.Text = "Titolo";
            // 
            // pubblica_recensione
            // 
            this.pubblica_recensione.Location = new System.Drawing.Point(139, 137);
            this.pubblica_recensione.Name = "pubblica_recensione";
            this.pubblica_recensione.Size = new System.Drawing.Size(392, 31);
            this.pubblica_recensione.TabIndex = 28;
            this.pubblica_recensione.Text = "Pubblica recensione";
            this.pubblica_recensione.UseVisualStyleBackColor = true;
            this.pubblica_recensione.Click += new System.EventHandler(this.pubblica_recensione_Click);
            // 
            // recensione_box
            // 
            this.recensione_box.Location = new System.Drawing.Point(139, 84);
            this.recensione_box.Name = "recensione_box";
            this.recensione_box.Size = new System.Drawing.Size(392, 47);
            this.recensione_box.TabIndex = 27;
            this.recensione_box.Text = "";
            // 
            // quantita_input
            // 
            this.quantita_input.Location = new System.Drawing.Point(14, 111);
            this.quantita_input.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.quantita_input.Name = "quantita_input";
            this.quantita_input.Size = new System.Drawing.Size(120, 20);
            this.quantita_input.TabIndex = 33;
            this.quantita_input.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // dettaglio_prodotto_4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.quantita_input);
            this.Controls.Add(this.punteggio_input);
            this.Controls.Add(this.titolo_box);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pubblica_recensione);
            this.Controls.Add(this.recensione_box);
            this.Controls.Add(this.aggiorna_recensioni);
            this.Controls.Add(this.recensioni_grid);
            this.Controls.Add(this.aggiunta_wishlist);
            this.Controls.Add(this.aggiunta_carrello);
            this.Controls.Add(this.label1);
            this.Name = "dettaglio_prodotto_4";
            this.Size = new System.Drawing.Size(545, 370);
            ((System.ComponentModel.ISupportInitialize)(this.recensioni_grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.punteggio_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantita_input)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView recensioni_grid;
        private System.Windows.Forms.Button aggiunta_wishlist;
        private System.Windows.Forms.Button aggiunta_carrello;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button aggiorna_recensioni;
        private System.Windows.Forms.NumericUpDown punteggio_input;
        private System.Windows.Forms.TextBox titolo_box;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button pubblica_recensione;
        private System.Windows.Forms.RichTextBox recensione_box;
        private System.Windows.Forms.NumericUpDown quantita_input;
    }
}
