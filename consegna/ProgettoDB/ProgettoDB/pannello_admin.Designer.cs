﻿
namespace ProgettoDB
{
    partial class pannello_admin
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.top_5_venduti = new System.Windows.Forms.Button();
            this.filtro_produttore = new System.Windows.Forms.Button();
            this.filtro_categoria = new System.Windows.Forms.Button();
            this.ricavato_mensile = new System.Windows.Forms.Button();
            this.elenco_prodotti = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.selettore_produttore = new System.Windows.Forms.ComboBox();
            this.selettore_categoria = new System.Windows.Forms.ComboBox();
            this.selettore_mese = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(119, 46);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(423, 321);
            this.dataGridView1.TabIndex = 5;
            // 
            // top_5_venduti
            // 
            this.top_5_venduti.Location = new System.Drawing.Point(3, 265);
            this.top_5_venduti.Name = "top_5_venduti";
            this.top_5_venduti.Size = new System.Drawing.Size(97, 41);
            this.top_5_venduti.TabIndex = 0;
            this.top_5_venduti.Text = "Top 5 venduti";
            this.top_5_venduti.UseVisualStyleBackColor = true;
            this.top_5_venduti.Click += new System.EventHandler(this.top_5_venduti_Click);
            // 
            // filtro_produttore
            // 
            this.filtro_produttore.Location = new System.Drawing.Point(3, 73);
            this.filtro_produttore.Name = "filtro_produttore";
            this.filtro_produttore.Size = new System.Drawing.Size(97, 38);
            this.filtro_produttore.TabIndex = 1;
            this.filtro_produttore.Text = "Filtra per produttore";
            this.filtro_produttore.UseVisualStyleBackColor = true;
            this.filtro_produttore.Click += new System.EventHandler(this.filtro_produttore_Click);
            // 
            // filtro_categoria
            // 
            this.filtro_categoria.Location = new System.Drawing.Point(3, 144);
            this.filtro_categoria.Name = "filtro_categoria";
            this.filtro_categoria.Size = new System.Drawing.Size(97, 41);
            this.filtro_categoria.TabIndex = 2;
            this.filtro_categoria.Text = "Filtra per categoria";
            this.filtro_categoria.UseVisualStyleBackColor = true;
            this.filtro_categoria.Click += new System.EventHandler(this.filtro_categoria_Click);
            // 
            // ricavato_mensile
            // 
            this.ricavato_mensile.Location = new System.Drawing.Point(3, 218);
            this.ricavato_mensile.Name = "ricavato_mensile";
            this.ricavato_mensile.Size = new System.Drawing.Size(97, 41);
            this.ricavato_mensile.TabIndex = 3;
            this.ricavato_mensile.Text = "Verifica ricavato mensile";
            this.ricavato_mensile.UseVisualStyleBackColor = true;
            this.ricavato_mensile.Click += new System.EventHandler(this.ricavato_mensile_Click);
            // 
            // elenco_prodotti
            // 
            this.elenco_prodotti.Location = new System.Drawing.Point(3, 312);
            this.elenco_prodotti.Name = "elenco_prodotti";
            this.elenco_prodotti.Size = new System.Drawing.Size(97, 41);
            this.elenco_prodotti.TabIndex = 4;
            this.elenco_prodotti.Text = "Elenco prodotti";
            this.elenco_prodotti.UseVisualStyleBackColor = true;
            this.elenco_prodotti.Click += new System.EventHandler(this.elenco_prodotti_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(127, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 25);
            this.label1.TabIndex = 6;
            this.label1.Text = "Pannello Admin";
            // 
            // selettore_produttore
            // 
            this.selettore_produttore.FormattingEnabled = true;
            this.selettore_produttore.Location = new System.Drawing.Point(3, 46);
            this.selettore_produttore.Name = "selettore_produttore";
            this.selettore_produttore.Size = new System.Drawing.Size(97, 21);
            this.selettore_produttore.TabIndex = 11;
            // 
            // selettore_categoria
            // 
            this.selettore_categoria.FormattingEnabled = true;
            this.selettore_categoria.Location = new System.Drawing.Point(3, 117);
            this.selettore_categoria.Name = "selettore_categoria";
            this.selettore_categoria.Size = new System.Drawing.Size(97, 21);
            this.selettore_categoria.TabIndex = 12;
            // 
            // selettore_mese
            // 
            this.selettore_mese.FormattingEnabled = true;
            this.selettore_mese.Location = new System.Drawing.Point(3, 191);
            this.selettore_mese.Name = "selettore_mese";
            this.selettore_mese.Size = new System.Drawing.Size(97, 21);
            this.selettore_mese.TabIndex = 13;
            // 
            // pannello_admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.selettore_mese);
            this.Controls.Add(this.selettore_categoria);
            this.Controls.Add(this.selettore_produttore);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.top_5_venduti);
            this.Controls.Add(this.elenco_prodotti);
            this.Controls.Add(this.filtro_produttore);
            this.Controls.Add(this.ricavato_mensile);
            this.Controls.Add(this.filtro_categoria);
            this.Controls.Add(this.dataGridView1);
            this.Name = "pannello_admin";
            this.Size = new System.Drawing.Size(545, 370);
            this.Load += new System.EventHandler(this.pannello_admin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button top_5_venduti;
        private System.Windows.Forms.Button filtro_produttore;
        private System.Windows.Forms.Button filtro_categoria;
        private System.Windows.Forms.Button ricavato_mensile;
        private System.Windows.Forms.Button elenco_prodotti;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox selettore_produttore;
        private System.Windows.Forms.ComboBox selettore_categoria;
        private System.Windows.Forms.ComboBox selettore_mese;
    }
}
