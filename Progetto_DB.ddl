-- *********************************************
-- * Standard SQL generation                   
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.1              
-- * Generator date: Dec  4 2018              
-- * Generation date: Fri Jul  2 18:12:44 2021 
-- * LUN file: C:\Users\nikol\Documents\Università\Basi di dati\Progetti_DBMain\Progetto_DB.lun 
-- * Schema: KIDZ/3 
-- ********************************************* 


-- Database Section
-- ________________ 


-- DBSpace Section
-- _______________


-- Tables Section
-- _____________ 

create table appartenenza (
     IDcategoria char(10) not null,
     IDprodotto char(10) not null,
     constraint IDappartenenza primary key (IDcategoria, IDprodotto));

create table CARRELLO (
     IDcarrello char(10) not null,
     codice_utente char(10) not null,
     totale numeric(38) not null,
     constraint IDCARRELLO primary key (IDcarrello),
     constraint FKpossiede_carrello_ID unique (codice_utente));

create table CARTA_DI_CREDITO (
     numero_carta numeric(16) not null,
     data_scadenza char(5) not null,
     cvv numeric(3) not null,
     constraint IDCARTA_DI_CREDITO_ID primary key (numero_carta));

create table CATEGORIA (
     IDcategoria char(10) not null,
     nome varchar(50) not null,
     constraint IDCATEGORIA primary key (IDcategoria));

create table composizione_carrello (
     IDprodotto char(10) not null,
     IDcarrello char(10) not null,
     constraint IDcomposizione_carrello primary key (IDcarrello, IDprodotto));

create table composizione_wishlist (
     IDwishlist char(10) not null,
     IDprodotto char(10) not null,
     constraint IDcomposizione_wishlist primary key (IDwishlist, IDprodotto));

create table DETTAGLIO_ORDINE (
     numero_linea numeric(38) not null,
     IDordine char(10) not null,
     IDprodotto char(10) not null,
     quantita numeric(38) not null,
     constraint IDDETTAGLIO_ORDINE primary key (IDordine, numero_linea),
     constraint IDDETTAGLIO_ORDINE_1 unique (IDordine, IDprodotto));

create table FATTURA (
     codice_fattura char(10) not null,
     IDordine char(10) not null,
     data_fattura date not null,
     importo_netto numeric(38) not null,
     IVA numeric(2) not null,
     codice_utente char(10) not null,
     constraint IDFATTURA primary key (codice_fattura),
     constraint FKcorrispettivo_ordine_ID unique (IDordine));

create table INDIRIZZO (
     IDindirizzo char(10) not null,
     nazione varchar(30) not null,
     citta varchar(30) not null,
     via varchar(50) not null,
     CAP numeric(10) not null,
     constraint IDINDIRIZZO_ID primary key (IDindirizzo));

create table intestazione_carta (
     numero_carta numeric(16) not null,
     codice_utente char(10) not null,
     constraint IDintestazione_carta primary key (codice_utente, numero_carta));

create table intestazione_indirizzo (
     IDindirizzo char(10) not null,
     codice_utente char(10) not null,
     constraint IDintestazione_indirizzo primary key (codice_utente, IDindirizzo));

create table NOTIFICA (
     IDnotifica char(10) not null,
     titolo varchar(50) not null,
     messaggio varchar(250) not null,
     data date not null,
     visualizzato char not null,
     constraint IDNOTIFICA primary key (IDnotifica));

create table ORDINE (
     IDordine char(10) not null,
     importo numeric(38) not null,
     email_ordine varchar(50) not null,
     data_ordine date not null,
     constraint IDORDINE_ID primary key (IDordine));

create table PRODOTTO (
     IDprodotto char(10) not null,
     nome_prodotto varchar(50) not null,
     prezzo_unitario numeric(38) not null,
     quantita_scorta numeric(38) not null,
     descrizione varchar(500) not null,
     taglia varchar(10) not null,
     eta numeric(3) not null,
     IDproduttore char(1) not null,
     constraint IDPRODOTTO_ID primary key (IDprodotto));

create table PRODUTTORE (
     IDproduttore char(1) not null,
     nome varchar(50) not null,
     constraint IDPRODUTTORE primary key (IDproduttore));

create table RECENSIONE (
     IDrecensione char(10) not null,
     IDprodotto char(10) not null,
     titolo varchar(50) not null,
     descrizione varchar(500) not null,
     punteggio numeric(1) not null,
     codice_utente char(10) not null,
     constraint IDRECENSIONE primary key (IDrecensione),
     constraint FKriferimento_recensione_ID unique (IDprodotto));

create table ricezione (
     IDnotifica char(10) not null,
     codice_utente char(10) not null,
     constraint IDricezione primary key (codice_utente, IDnotifica));

create table riferimento_notifica (
     IDnotifica char(10) not null,
     IDprodotto char(10) not null,
     constraint FKrif_NOT_ID primary key (IDnotifica));

create table UTENTE (
     codice char(10) not null,
     nome varchar(50) not null,
     cognome varchar(50) not null,
     email varchar(50) not null,
     password varchar(20) not null,
     telefono numeric(20) not null,
     admin char not null,
     constraint IDUTENTE_ID primary key (codice));

create table WISHLIST (
     IDwishlist char(10) not null,
     codice_utente char(10) not null,
     constraint IDWISHLIST primary key (IDwishlist),
     constraint FKpossiede_wishlist_ID unique (codice_utente));


-- Constraints Section
-- ___________________ 

alter table appartenenza add constraint FKapp_PRO
     foreign key (IDprodotto)
     references PRODOTTO;

alter table appartenenza add constraint FKapp_CAT
     foreign key (IDcategoria)
     references CATEGORIA;

alter table CARRELLO add constraint FKpossiede_carrello_FK
     foreign key (codice_utente)
     references UTENTE;

alter table composizione_carrello add constraint FKcom_CAR
     foreign key (IDcarrello)
     references CARRELLO;

alter table composizione_carrello add constraint FKcom_PRO
     foreign key (IDprodotto)
     references PRODOTTO;

alter table composizione_wishlist add constraint FKcom_PRO_1
     foreign key (IDprodotto)
     references PRODOTTO;

alter table composizione_wishlist add constraint FKcom_WIS
     foreign key (IDwishlist)
     references WISHLIST;

alter table DETTAGLIO_ORDINE add constraint FKcomposizione_ordine
     foreign key (IDordine)
     references ORDINE;

alter table DETTAGLIO_ORDINE add constraint FKriferimento_ordine
     foreign key (IDprodotto)
     references PRODOTTO;

alter table FATTURA add constraint FKintestazione_ordine
     foreign key (codice_utente)
     references UTENTE;

alter table FATTURA add constraint FKcorrispettivo_ordine_FK
     foreign key (IDordine)
     references ORDINE;

alter table intestazione_carta add constraint FKint_UTE_1
     foreign key (codice_utente)
     references UTENTE;

alter table intestazione_carta add constraint FKint_CAR
     foreign key (numero_carta)
     references CARTA_DI_CREDITO;

alter table intestazione_indirizzo add constraint FKint_UTE
     foreign key (codice_utente)
     references UTENTE;

alter table intestazione_indirizzo add constraint FKint_IND
     foreign key (IDindirizzo)
     references INDIRIZZO;

alter table PRODOTTO add constraint FKproduzione
     foreign key (IDproduttore)
     references PRODUTTORE;

alter table RECENSIONE add constraint FKscrittura
     foreign key (codice_utente)
     references UTENTE;

alter table RECENSIONE add constraint FKriferimento_recensione_FK
     foreign key (IDprodotto)
     references PRODOTTO;

alter table ricezione add constraint FKric_UTE
     foreign key (codice_utente)
     references UTENTE;

alter table ricezione add constraint FKric_NOT
     foreign key (IDnotifica)
     references NOTIFICA;

alter table riferimento_notifica add constraint FKrif_NOT_FK
     foreign key (IDnotifica)
     references NOTIFICA;

alter table riferimento_notifica add constraint FKrif_PRO
     foreign key (IDprodotto)
     references PRODOTTO;

alter table WISHLIST add constraint FKpossiede_wishlist_FK
     foreign key (codice_utente)
     references UTENTE;


-- Index Section
-- _____________ 

