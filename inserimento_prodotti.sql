INSERT INTO `prodotto`(`IDprodotto`, `nome_prodotto`, `prezzo_unitario`, `quantita_scorta`, `descrizione`, `taglia`, `eta`, `IDproduttore`) VALUES

('02A0000', 'Cassa Barbie', 35.00, 8, 'Il registratore di cassa di Barbie ha tanti accessori ed effetti sonori: giocare a fare shopping con le tue amiche non è mai stato così divertente!', 'U', 5, '1'),

('03A0000', 'Barbie Bambola', 28.00, 5, 'Crea stili spumeggianti e colorati per i capelli di questa bambola Barbie!
I giovani stilisti adesso possono creare tantissimi look alla moda con la bambola Barbie Capelli Arcobaleno, dai capelli biondi, lunghi addirittura 19 cm!', 'U', 5, '1'),

('04A0000', 'Elsa classic', 17.00, 5, 'costume di ottima qualità, curato nei dettagli, sarai la più bella, una vera e propria principessa.', 'U', 4, '2'),

('05A0000', 'Costume Elsa e Anna', 63.00, 4, ' Questo cofanetto regalo di Frozen 2 è composto da due costumi di Elsa e Anna per bambina in licenza ufficiale. Il costume di anna è composto da un vestito con cintura e mantello (scarpe e parrucca non incluse). Il costume di Elsa è composto da un vestito azzurro ed un mantello trasparente. Sarà un confanetto regalo di Frozen 2 perfetto per carnevale o per una festa di compleanno a tema Frozen!', 'U', 4, '2'),

('06A0000', 'Casco Stormtrooper', 64.00, 10, 'Ricrea ogni dettaglio iconico di un casco Stormtrooper con questo modello da esposizione in mattoncini, che fa parte di una serie LEGO® Star Wars™ da collezione per adulti.', 'U', 18, '4'),

('07A0000', 'Casco Boba Fett', 64.00, 15, 'Crea un fantastico casco Boba Fett, parte di una serie di modelli da costruire ed esporre Star Wars™ di realizzati con mattoncini LEGO® per adulti e qualsiasi costruttore avanzato.', 'U', 18, '4'),

('08A0000', 'Yoda', 109.00, 3, 'Partecipa ad avvincenti battaglie Star Wars™ con il Maestro Jedi Yoda costruibile! Sarà anche fatto di mattoncini LEGO®, ma con la sua potente spada laser questo grande personaggio LEGO di Yoda è una forza da non sottovalutare. Muovi la testa, le sopracciglia, le dita delle mani e dei piedi per metterlo in una vera posa da Maestro Jedi! Questo set contiene anche un espositore e una piccola minifigure di Yoda con spada laser.', 'U', 18, 4)